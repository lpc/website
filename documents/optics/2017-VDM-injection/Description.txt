<h3>Data used</h3>
<table>
<tr><td>Repository used </td><td>:</td><td>db -> /afs/cern.ch/eng/lhc/optics/runII/2016    </td></tr>
<tr><td>sequence        </td><td>:</td><td>db/lhc_as-built.seq                             </td></tr>
<tr><td>tools           </td><td>:</td><td>db/toolkit/slice.madx                           </td></tr>
<tr><td>optics          </td><td>:</td><td>opt_inj_thin_cs.madx based on opt_inj_thin.madx </td></tr>
</table>

<h3>Description</h3>

Optics for the VdM scan in the low energy high beta star special run.
<ul>
<li>Based on 2016 injection optics.</li>
<li>No crossing angle in IP1&5.</li>
<li>Separation in IP 2 and 8 set to 3.5mm. (No separation in IP2&5.)</li>
<li>Beam energy set to 450GeV in beamDefinition and opticsfile.</li>
<li>The knob on_ov5 does not work with the 2016 optics file above. This bump was not yet implemented in 2016. Therefore no vertical bump for CMS.</li>
<li>on_alice and on_lhcb have been changed to 7.0/0.45 since we are working at injection energy.</li>
</li>
