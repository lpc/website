#!/usr/bin/python

import urllib2

year = "2017"

baseurl = "http://abpdata.web.cern.ch/abpdata/lhc_optics_web/www/opt" + year + "/coll400/"

twissfiles = ["twiss_lhcb1.tfs","twiss_lhcb2.tfs"]
for ir in range( 1, 9 ):
    twissfiles.append( "twiss_ir" + str(ir) + "b1.tfs" )
    twissfiles.append( "twiss_ir" + str(ir) + "b2.tfs" )

for tf in twissfiles:
    print "Downloading " + tf
    url = baseurl + tf
    resp = urllib2.urlopen( url )
    contents = resp.read()
    fd = open( tf, "w" )
    fd.write( contents )
    fd.close()

