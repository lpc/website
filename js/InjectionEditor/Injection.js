Injection = function() {
    
    this.beamNo        = -1;
    this.selected      = false;
    this.lhcbunch      = -1;            // the bunch where the batch starts in lhc
    this.batchSpacing  = 0;
    this.noBunches     = 0;
    this.bunchArray    = new Array();  // 0: empty, 1: bunch
    this.batches       = new Array();
    this.injectionType = "nominal";
    return this;
};

Injection.prototype.toggleType = function( type ) {
    console.log( this.injectionType );
    if ( this.injectionType == "nominal" ) {
	this.injectionType = "intermediate";
    } else if ( this.injectionType == "intermediate" ) {
	this.injectionType = "nominal";
    } else {
	console.log( "ERROR (Injection.toggleType) ===> forbidden injection type encountered: " , this.injectionType );
	console.log( "Forcing to nominal" );
	this.injectionType = "nominal";
    }
    console.log( this.injectionType );
}

Injection.prototype.clone = function() {
    ni               = new Injection();

    ni.beamNo        = this.beamNo;
    ni.selected      = this.selected;
    ni.lhcbunch      = this.lhcbunch;
    ni.batchSpacing  = this.batchSpacing;
    ni.noBunches     = this.noBunches;
    ni.bunchArray    = this.bunchArray.slice(0);
    ni.injectionType = this.injectionType;
    
    for ( var ix in this.batches ) {
        ni.batches.push(this.batches[ix].clone());
    }

    return ni;
}



Injection.prototype.addBatch = function( batch, bunchNo ) {

    batch.injBunch = bunchNo;

    // Fill the injection bunchArray with 0 up to the bx where we add the bunch (bunchNo)
    var bx = this.bunchArray.length; // the next bx to be filled

    for ( var i = bx; i < bunchNo; i++ ) {
        this.bunchArray.push(0);        
    }

    // Copy the batch array over
    for ( var i=0; i<batch.getlength(); i++ ) {
        this.bunchArray.push( batch.bunchArray[i] );
        if ( batch.bunchArray[i] == 1 )
            this.noBunches++;
    }

    
    // also fill the array with clones of the batches.
    this.batches.push( batch.clone() );

}



// spacing is given in ns: Minimum is 25
Injection.prototype.addBatches = function( batch, nbatch, spacing ) {
    var nbatch = typeof nbatch !== 'undefined' ? nbatch : 1;

    spacing = spacing / 25 - 1;
    this.batchSpacing = spacing;

    injBunch = 0;
    

    for ( var i = 0; i < nbatch; i++ ) {

        // fill the bunchArray
        // If this is not the first batch put '0' for the spacing
        if ( i > 0 ) {
	    for ( var j = 0; j < spacing; j++ ) {
		this.bunchArray.push(0);
                injBunch++;
	    }
	}
        
        batch.injBunch = injBunch;
        for ( var j = 0; j < batch.getlength(); j ++ ) {
            this.bunchArray.push( batch.bunchArray[j] );
            if ( batch.bunchArray[j] == 1 )
                this.noBunches++;
            injBunch++;
        }

        // also fill the array with clones of the batches.
	this.batches.push( batch.clone() );
    }

    
    return this;
}



Injection.prototype.getLength = function() {
    return this.bunchArray.length;
}

