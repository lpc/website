var editorState = "changed";
var phaseTimer = undefined;

InjectionDisplay = function( id ) {

    //debug("Display created");
    this.id  = id;
    
    // the autoinjector is needed for single injections
    this.autoInjector = new AutoInjector();
    
    this.down   = {}; // converted coordinates of mouse down event    
    this.up     = {}; // converted coordinates of mouseup event    
    this.curInj = {}; // the currntly configured injection (for adding)
    
    this.resize();

    var self = this;

    $(window).resize( function() {
        self.resize();
        redraw();
    });
    $(document).keydown( function( event ) { return self.doKeyDown( event ); } );
    $('canvas#'+id+'_overlay').mousedown( function( event ) { return self.doMouseDown( event ); } );
    $('canvas#'+id+'_overlay').mouseup(   function( event ) { return self.doMouseUp( event ); } );
    // For moving selections
    $('canvas#'+id+'_overlay').mousemove( function( event ) { return self.doMouseMove( event ); } );
    // For adding new injections
    $('canvas#'+id+'_overlay').mousemove( function( event ) { return self.doMouseMoveOv( event ); } );
}

InjectionDisplay.prototype.change = function() {
    if ($('#continuousUpdate').prop('checked') == true ){
        editorState = "changed";
        if (phaseTimer) {
            clearTimeout(phaseTimer);
        }
        phaseTimer = setTimeout( fullDetuning, 2000 );
    }    
}

InjectionDisplay.prototype.resize = function() {

    var wwidth = window.innerWidth;
    this.zoom = (wwidth - 30) / 3564;

    var quarter = this.zoom * 3564;
    this.canvas        = document.getElementById( this.id );
    this.context       = this.canvas.getContext('2d');
    this.canvas.width  = quarter + 10;
    this.canvas.height = 290;

    this.overlay        = document.getElementById( this.id + '_overlay');
    this.ocontext       = this.overlay.getContext('2d');
    this.overlay.width  = quarter + 10;
    this.overlay.height = 290;
    
    $(this.overlay).offset( $(this.canvas).offset() );
    
}



InjectionDisplay.prototype.unselect = function() {

    for( var i = 0; i < lhc.beam1.injections.length; i++ ) {
        lhc.beam1.injections[i].selected = false;
    }
    for( var i = 0; i < lhc.beam2.injections.length; i++ ) {
        lhc.beam2.injections[i].selected = false;
    }

    redraw();

    $('div#maxmove').html( "" );
    
    return false;

}

InjectionDisplay.prototype.convertXY = function( x, y ) {

    var crect = $('canvas')[0].getBoundingClientRect();

    res = { beam : 0,
            bx   : 0,
            rawx : x,
            rawy : y };
    x = x - crect.left - 5;
    y = y - crect.top;
    
    res.x = x;
    res.y = y;
    
    if ( y >= 5 && y <= 55 )
        res.beam = 1;

    if ( y > 65 && y <= 115 )
        res.beam = 2;

    res.bx = Math.min( x / this.zoom, 3564 ) - 1;
    res.bx = parseInt( Math.max( 0, res.bx ) );
    //debug( res.x, res.y, res.bx);
    return res

}




InjectionDisplay.prototype.doMouseDown = function( e ) {

    //debug("mouse down " + e.target + e.currentTarget + e.delegateTarget + event.timeStamp);
    if ( typeof lhc == 'undefined' ) return;
    if ( e.which != 1 ) return;
    
    e.stopPropagation();

    var inp = getInput();

    if ( this.state == "addInjection" ) {
        if ( this.curInj.possible ) {

            // need to add the injection
            if (inp.fillAlgo == "proton" && inp.lhcbripple) {
                this.autoInjector.rippleInject( this.curInj.injection, this.curInj.bx, -894 );
            } else if( inp.fillAlgo == "protonLead" && inp.qripple ) {
                this.autoInjector.rippleInject( this.curInj.injection, this.curInj.bx, -891 );
            } else {
                var inj = this.curInj.injection.clone();
                this.curInj.beam.inject( inj, this.curInj.bx );
            }
            redraw();

            this.dumpCollisions($('div#res'), $('div#longrange'));

            this.change();
        }
        
        this.resetState();
        return;

    }

    var x = e.clientX;
    var y = e.clientY;

    res   = this.convertXY( x, y );

    if ( e.shiftKey ) {

        this.state = "move";
        this.integ = 0;

    } else {

        this.state = "select"

    }
    
    this.down = res;

    return false;

}




InjectionDisplay.prototype.doMouseUp = function( e ) {

    //debug("mouseup " + e.target + e.which);
    if ( typeof lhc == 'undefined'  ) return;
    if ( e.which != 1 ) return;

    e.stopPropagation();

    if (this.state == "reset")
        return;

    var x      = e.clientX;
    var y      = e.clientY;
    var state  = this.state;
    this.state = "reset";

    if (state == "move") {

        this.b1selmax = lhc.beam1.maxSelectionMove();
        this.b2selmax = lhc.beam2.maxSelectionMove();
        this.sellimits = {
            "forwards" : Math.min( this.b1selmax.forwards, this.b2selmax.forwards ),
            "backwards" : Math.min( this.b1selmax.backwards, this.b2selmax.backwards )
        };

        $('div#maxmove').html( "max forward: " + this.sellimits.forwards
                               + " &nbsp;&nbsp; max backward: "
                               + this.sellimits.backwards );

        this.change();
        return;

    }

    this.up = this.convertXY( x, y );

    // if the mouse down was not in a valid region don't proceed
    if ( this.down.beam == 0 )
        return false;

    var b1  = lhc.beam1;
    var b2  = lhc.beam2;
    var inj = undefined;
    
    if ( this.down.beam == 1 || this.up.beam == 1 ) {
        //debug( "select 1");
        this.selectBeam( b1 );

    }

    if ( this.down.beam == 2 || this.up.beam == 2 ) {
        //debug( "select 2");
        this.selectBeam( b2 );

    }

    redraw();

    this.b1selmax  = b1.maxSelectionMove();
    this.b2selmax  = b2.maxSelectionMove();
    this.sellimits = {
        "forwards" : Math.min( this.b1selmax.forwards, this.b2selmax.forwards ),
        "backwards" : Math.min( this.b1selmax.backwards, this.b2selmax.backwards )
    };

    if ( this.sellimits.forwards == 999999 )
        $('div#maxmove').html( "" )
    else
        $('div#maxmove').html( "max forward: " + this.sellimits.forwards
                               + " &nbsp;&nbsp; max backward: " + this.sellimits.backwards );

    return false;

}





InjectionDisplay.prototype.doMouseMoveOv = function( e ) {

    if (this.state != "addInjection" )
        return;

    var inp = getInput();

    this.ocontext.save();
    this.ocontext.translate(5,5);

    var x = e.clientX;
    var y = e.clientY;

    var pos = this.convertXY(x,y)
    
    var ybas = 0;
    var beam;
    if (pos.beam == 2) {
        ybas = 60;
        beam = lhc.beam2;
    } else if( pos.beam == 1) {
        beam = lhc.beam1;
    } else {
        this.ocontext.clearRect(0,0,this.overlay.width, this.overlay.height);
        this.startx = -1;
        this.ocontext.restore();
        return;
    }

    var bx = pos.bx;

    if ( this.startx == -1 ) {

        this.startx  = x;
        this.startbx = bx;
	// Is this needed here??? only used during selection
        this.integ   = 0;

    } else {
        
        var deltabx = parseInt( (x - this.startx) / (inp.shiftspeed * this.zoom ));
        bx = this.startbx + deltabx;

    }


    // CHECK if we can insert the injection here.
    //backwards:
    var possible = true;
    for (var i = bx; (i >= 0) && (i >= (bx - this.curInj.injspacing + 1 )); i--) {
        if (beam.bunches[i] != 0) {
            possible = false;
            break;
        }
    }
    //forwards
    if ( possible ) {
        for (var i = bx+1; i < Math.min( lhc.agk + 1, bx + this.curInj.length + this.curInj.injspacing - 1 ); i++) {
            if (beam.bunches[i] != 0) {
                possible = false;
                break;
            }
        }
    }

    if (bx > lhc.agk)
        possible = false;

    if ( bx + this.curInj.length - 1 > 3442 ) {
        error( "You leak into the about gap !!!");
        possible = false;
    } else {
        clerror();        
    }

    this.curInj.beam = beam;
    this.curInj.bx   = bx;
    
    this.ocontext.lineWidth = 4;
    if (possible) {
        this.ocontext.strokeStyle = "#008000";
        this.curInj.possible = true;
    } else {
        this.ocontext.strokeStyle = "#202020";
        this.curInj.possible = false;
    }
    
    this.ocontext.clearRect(0,0,this.overlay.width, this.overlay.height);
    this.ocontext.beginPath();
    this.ocontext.rect( bx * this.zoom, ybas, this.zoom*this.curInj.length, 48 );
    this.ocontext.stroke();

    $('div#moveX').html( "BX of injection: " + bx + '<br>');

    this.ocontext.restore();
}

InjectionDisplay.prototype.addAdvancedInjection = function() {
    var i_sel = $('select#advancedInjections').val();
    if ( i_sel == "" ) {
        error("Select an advanced injection from the drop-down list first");
        return;
    }
    clerror();
    this.state = "addInjection";
    this.ocontext.clearRect(0,0,this.overlay.width, this.overlay.height);
    var inp       = getInput();
    var injection = new Injection();
    // Make the injection
    var inj = injectionEditor.patternHash[i_sel]
    for( var i=0; i<inj.batches.length; i++ ) {
        var ba = new Batch( inj.batches[i].pattern );
        injection.addBatch( ba, inj.batches[i].first );
    }
    if ( inj.batches.length > 1 ) {
        //console.log( " orig batchspacing " , inj.batchSpacing );
        injection.batchSpacing = inj.batchSpacing - 1;
    }

    this.curInj = { length     : injection.getLength(),
                    injspacing : parseInt( inp.injSpacing/25),
                    injection  : injection,
                    possible   : false };
}

InjectionDisplay.prototype.addInjection = function() {
    this.state = "addInjection";

    this.ocontext.clearRect(0,0,this.overlay.width, this.overlay.height);

    var inp       = getInput();

    var batch     = new Batch( inp.batchLength, inp.bunchSpacing );
    var injection = new Injection();

    injection.addBatches( batch, inp.noBatches, inp.batchSpacing );

    this.curInj = { length     : injection.getLength(),
                    injspacing : parseInt(inp.injSpacing/25),
                    injection  : injection,
                    possible   : false };
}

InjectionDisplay.prototype.doKeyDown = function( e ) {
    var code = e.which;
    var deltabx = 0;
    if ( code == '37') deltabx = -1;
    if ( code == '39') deltabx = 1;
    if (deltabx == 0) return;
    if (this.sellimits === undefined) return;
    if (this.sellimits.forwards == 999999) return;
    var focusElem = $(document.activeElement);
    if (focusElem.context.id != "" ) return;
    if (deltabx > 0 ) {

        deltabx = Math.min( deltabx, this.sellimits.forwards );

    } else {

        deltabx = Math.max( deltabx, -1 * this.sellimits.backwards );

    }

    lhc.moveSelection( deltabx );

    this.b1selmax = lhc.beam1.maxSelectionMove();
    this.b2selmax = lhc.beam2.maxSelectionMove();
    this.sellimits = {
        "forwards" : Math.min( this.b1selmax.forwards, this.b2selmax.forwards ),
        "backwards" : Math.min( this.b1selmax.backwards, this.b2selmax.backwards )
    };

    $('div#maxmove').html( "max forward: " + this.sellimits.forwards
                           + " &nbsp;&nbsp; max backward: "
                           + this.sellimits.backwards );

    this.change();

    $('#moveX').html( deltabx );

    redraw();
}

InjectionDisplay.prototype.doMouseMove = function( e ) {

    if (this.state != "move" )
        return;

    var inp = getInput();

    var x       = e.clientX;
    var delta   = x - this.down.rawx;
    var deltabx = parseInt(delta / (inp.shiftspeed * this.zoom));
    if (deltabx > 0 ) {

        deltabx = Math.min( deltabx, this.sellimits.forwards );

    } else {

        deltabx = Math.max( deltabx, -1 * this.sellimits.backwards );

    }

    var delta   = deltabx - this.integ;
    this.integ += delta;

    lhc.moveSelection( delta );

    //this.dumpCollisions($('div#res'));

    $('#moveX').html( deltabx );
    
    redraw();
}



InjectionDisplay.prototype.drawBeamStructure = function() {

    if ( typeof lhc == 'undefined' ) return;

    var quarter = 3564 * this.zoom; // not a quarter but all!

    this.context.clearRect(0,0,this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(5,5);
    
    this.context.strokeStyle = "rgba( 50,50,50,0.5 )";
    this.context.rect( 0,0,quarter,50);
    this.context.rect( 0,160,quarter,50);
    this.context.rect( 0,220,quarter,50);
    this.context.rect( 0,60,quarter,50);
    this.context.stroke();

    this.context.fillStyle = "rgba( 50,50,50,0.5 )";
    this.context.fillRect( parseInt(lhc.AGK * this.zoom/10), 0,parseInt( (34421 - lhc.AGK) * this.zoom/10),50)
    this.context.fillRect( parseInt(lhc.AGK * this.zoom/10),60,parseInt( (34421 - lhc.AGK) * this.zoom/10),50)
    this.context.stroke();

    this.context.fillStyle = "rgba( 150,00,00,0.5 )";
    this.context.fillRect( parseInt(34431 * this.zoom/10),0,parseInt(quarter - 34421 * this.zoom/10),50)
    this.context.fillRect( parseInt(34431 * this.zoom/10),60,parseInt(quarter - 34421 * this.zoom/10),50)
    this.context.stroke();

    var q = 3564 * this.zoom / 4;

    this.context.strokeStyle = "rgba( 50,50,50,0.5 )";

    for( var i=1; i<=3; i++ ) {

        this.context.beginPath();
        this.context.moveTo( q*i, 0 );
        this.context.lineTo( q*i, 110);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo( q*i, 160 );
        this.context.lineTo( q*i, 270);
        this.context.stroke();

    }
    
    this.context.restore();

}


InjectionDisplay.prototype.toggleInjectionType = function() {
    lhc.beam1.toggleInjectionTypeSelected();
    lhc.beam2.toggleInjectionTypeSelected();
    redraw();
}

InjectionDisplay.prototype.deleteSelection = function() {

    lhc.beam1.deleteSelected();
    lhc.beam2.deleteSelected();    

    redraw();
    
    this.dumpCollisions($('div#res'), $('div#longrange'));

    this.change();
}



InjectionDisplay.prototype.resetState = function() {

    this.state = "reset";

    this.ocontext.clearRect(0,0,this.overlay.width, this.overlay.height);

}



InjectionDisplay.prototype.selectBeam = function( b ) {
    var i = -1;
    var inj = undefined;
    do {
        i += 1;
        if (i == b.injections.length )
            break;
        inj = b.injections[i];
    } while ( inj.lhcbunch + inj.getLength() <= this.down.bx );

    while( inj && (inj.lhcbunch <= this.up.bx) ) {
        if ( inj.selected )
            inj.selected = false;
        else
            inj.selected = true;
        i++
        if ( i == b.injections.length )
            break;
        inj = b.injections[i];
    }
}


InjectionDisplay.prototype.drawB1 = function( ) {
    voffset = 160; // for the JJowett display
    this.context.save();
    this.context.translate(5,5);
    var b1 = lhc.beam1;
    var b2 = lhc.beam2;
    //debug("dawB1");
    for (var i=0; i<b1.injections.length; i++ ) {
        //debug("inj");
        var inj = b1.injections[i];

	var injType = inj.injectionType;
        
        for (var j = 0; j<inj.batches.length; j++ ) {
            var ba = inj.batches[j]
            //var busp = ba.bunch_spacing / 25;
            var xp = (inj.lhcbunch + ba.injBunch) * this.zoom;
            var length = ba.getlength() * this.zoom;

            //// This assumes we should add empty slots for bunch spacing larger than 25ns
            //// Probably not correct for exotic scenarios where mixed spacings are requested.
            //// var length = (ba.bunches * busp - (busp -1 ) ) * this.zoom;
            // minimal length is 3 for visibility
            length = Math.max( length, 1 );
            this.context.fillStyle = 'blue';
            if (inj.selected) {
                this.context.fillStyle = "rgba( 0,0,255,1.0 )";
            } else {
                this.context.fillStyle = "rgba( 0,0,255,0.5 )";
            }

	    if ( injType == "intermediate" ) {
		this.context.fillRect(xp,0,length,22);
		this.context.fillRect(xp,28,length,22);
	    } else {
		this.context.fillRect(xp,0,length,50);
	    }

            // try to put something for LHCb in b2
            var xlb = (inj.lhcbunch + ba.injBunch - 894);
            if ( xlb + parseInt(ba.bunches) < 0 )
                xlb = xlb + 3564;
            xlb = xlb * this.zoom;
            this.context.fillStyle = 'rgba( 00,70,00,0.5)';
            this.context.fillRect(xlb, 60, length, 18 );

            // try to put somethign for ALICE in b2
            var xlb = (inj.lhcbunch + ba.injBunch + 891);
            if ( xlb + parseInt(ba.bunches) > 3563 )
                xlb = xlb - 3564;
            xlb = xlb * this.zoom;
            this.context.fillStyle = 'rgba( 70,00,00,0.5)';
            this.context.fillRect(xlb, 92, length, 18 );
        }


    }

    // sets the bunch classes
    lhc.countCollisions();
    var bucl1 = b1.bunchClasses;
    for ( var ix=0; ix<bucl1.length; ix++ ){
	if ( bucl1[ix] == -1 ) continue;
        var xp = ix * this.zoom;
        if (bucl1[ix] == 0 ) {
            this.context.strokeStyle = 'rgba(0,0,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 168 );
            this.context.lineTo( xp, 202 );
            this.context.stroke();	    
	    continue; // no other bit tests since -1 sets all bits!!
	}
        if ( bucl1[ix] & 4 ) {
            this.context.strokeStyle = 'rgba(0,220,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 160 );
            this.context.lineTo( xp, 177 );
            this.context.stroke();
        }
        if ( bucl1[ix] & 2 ) {
            this.context.strokeStyle = 'rgba(200,0,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 177 );
            this.context.lineTo( xp, 194 );
            this.context.stroke();
        }
        if ( bucl1[ix] & 1 ) {
            this.context.strokeStyle = 'rgba(0,0,200,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 194 );
            this.context.lineTo( xp, 210 );
            this.context.stroke();
        }
    }
    
    var bucl2 = b2.bunchClasses;
    for ( var ix=0; ix<bucl2.length; ix++ ){
	if ( bucl2[ix] == -1 ) continue;
        var xp = ix * this.zoom;
        if (bucl2[ix] == 0 ) {
            this.context.strokeStyle = 'rgba(0,0,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 228 );
            this.context.lineTo( xp, 262 );
            this.context.stroke();	    
	    continue; // no other bit tests since -1 sets all bits!!
	}
        if ( bucl2[ix] & 4 ) {
            this.context.strokeStyle = 'rgba(0,220,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 220 );
            this.context.lineTo( xp, 237 );
            this.context.stroke();
        }
        if ( bucl2[ix] & 2 ) {
            this.context.strokeStyle = 'rgba(200,0,0,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 237 );
            this.context.lineTo( xp, 254 );
            this.context.stroke();
        }
        if ( bucl2[ix] & 1 ) {
            this.context.strokeStyle = 'rgba(0,0,200,1.0)';
            this.context.beginPath();
            this.context.moveTo( xp, 254 );
            this.context.lineTo( xp, 270 );
            this.context.stroke();
        }
    }
    
    for (var i=0; i<b2.injections.length; i++ ) {

        var inj = b2.injections[i];

	var injType = inj.injectionType;

        for ( var j=0; j<inj.batches.length; j++ ) {
            var ba = inj.batches[j];
            //var busp = ba.bunch_spacing / 25;
            var xp = (inj.lhcbunch + ba.injBunch) * this.zoom;
            var length = ba.getlength() * this.zoom;
            //console.log( "beam2 batch length " , length )
            //var length = (ba.bunches * ( 1 + busp - 1) - (busp -1 ) ) * this.zoom;
            length = Math.max( length, 1 );
            if (inj.selected) {
                this.context.fillStyle = 'rgba(255,0,0,1.0)';
            } else {
                this.context.fillStyle = 'rgba(255,0,0,0.5)';
            }

	    if ( injType == "intermediate" ) {
		this.context.fillRect(xp,60,length,22);
		this.context.fillRect(xp,88,length,22);
	    } else {
		this.context.fillRect(xp,60,length,50);
	    }

            // try to put somethign for LHCb in b1
            var xlb = (inj.lhcbunch + ba.injBunch + 894);
            if ( xlb + parseInt(ba.bunches) > 3563 )
                xlb = xlb - 3564;
            xlb = xlb * this.zoom;
            this.context.fillStyle = 'rgba( 00,70,00,0.5)';
            this.context.fillRect(xlb, 0, length, 18 );

            // try to put somethign for ALICE in b1
            var xlb = (inj.lhcbunch + ba.injBunch - 891);
            if ( xlb + parseInt(ba.bunches) < 0 )
                xlb = xlb + 3564;
            xlb = xlb * this.zoom;
            this.context.fillStyle = 'rgba( 70,00,00,0.5)';
            this.context.fillRect(xlb, 32, length, 18 );
        }
    }
    
    this.context.restore();
}

InjectionDisplay.prototype.dumpCollisions = function( el, lrel ) {
    var res = lhc.countCollisions();

    colstr = '<table class="schemeinfo"><tbody><tr><td>';
    colstr += '<table class="beaminfo"><thead><tr><th colspan="2">Beam Info</th></tr></thead><tbody>';
    colstr += '<tr><td>Bunches B1/B2</td><td>' + res['NB1'] + ' / ' + res['NB2'] + '</td></tr>';
    colstr += '<tr><td>Injections B1/B2</td><td>' + lhc.beam1.injections.length + ' / '
        + lhc.beam2.injections.length + '</td></tr>';
    colstr += '</tbody></table>';
    colstr += '</td><td>';
    colstr += '<table class="beaminfo"><thead><tr><th colspan="2">Collisions</th></tr></thead><tbody>';
    colstr += '<tr><td>ATLAS/CMS</td><td>' + res['ATLAS/CMS'] + '</td></tr>';
    colstr += '<tr><td>ALICE</td><td>' + res['ALICE'] + ' ('
        + (100*res['ALICE']/res['ATLAS/CMS']).toPrecision(3) + '%)</td></tr>';
    colstr += '<tr><td>LHCb</td><td>' + res['LHCb'] + ' ('
        + (100*res['LHCb']/res['ATLAS/CMS']).toPrecision(3) + '%)</td></tr>';
    colstr += '<tr><td>Non Colliding B1</td><td>' + res['NC1'] + '</td></tr>';
    colstr += '<tr><td>Non Colliding B2</td><td>' + res['NC2'] + '</td></tr>';
    colstr += '</tbody></table>';

    colstr += '</td></tr>';

    colstr += '<tr><td colspan="2">';
    colstr += '<p>B1 classes : ';
    for (var ix=0; ix<res['B1_classes'].length; ix++) {
        colstr += ix + ":" + res['B1_classes'][ix] + "&nbsp;&nbsp;&nbsp;";
    }
    colstr += '<p></p>B2 classes : ';
    for (var ix=0; ix<res['B2_classes'].length; ix++) {
        colstr += ix + ":" + res['B2_classes'][ix] + "&nbsp;&nbsp;&nbsp;";
    }
    colstr += '</p>';
    colstr += '</td></tr>';

    colstr += '<tr><td colspan="2">';

    colstr +=  '<table class="beaminfo"><thead><tr><th>Injections</th></tr></thead><tbody><tr><td><table class="injections"><tbody><tr><th>B1</th><td></td>';
    var sp1 = 0;
    var waste = 0;
    if ( lhc.beam1.injections.length > 0 ) {
        sp1 = lhc.beam1.injections[0].lhcbunch;
        waste = lhc.beam1.injections[0].lhcbunch;
    }
    var spacstr = '<tr><td>empty</td><td>' + sp1 + '</td>';

    //var injedstr_b1 = "";
    var injhash_b1 = {};
    var maxlenb1 = 0;
    var maxlenb2 = 0;
    var maxilen = 0;
    for (var i=0; i<lhc.beam1.injections.length; i++) {

        var inj = lhc.beam1.injections[i];

        maxlenb1 = Math.max( maxlenb1, inj.getLength() );

        var noba = inj.batches.length;
        var babu = inj.noBunches / noba;
        var key = noba + " x " + babu;

        if (! (key in injhash_b1) ) {
            injhash_b1[key] = {'positions' : [],
                               'nbatch'    : noba,
                               'nbunch'    : babu,
                               'batch_sp'  : inj.batchSpacing,
                               'bunch_sp'  : inj.batches[0].bunch_spacing }; // take the first batch
        }
        
        colstr += '<td>' + inj.lhcbunch + "</td>" ;
        injhash_b1[key]['positions'].push( inj.lhcbunch );

        if ( i < lhc.beam1.injections.length-1 ) {
            var space = lhc.beam1.injections[i+1].lhcbunch - inj.lhcbunch
                - inj.getLength() + 1;
            spacstr +=  '<td>' + space + '</td>';
            waste += space - lhc.injSpacing;
        } else {
            waste += lhc.agk - inj.lhcbunch;
            spacstr += "<td>" + (lhc.agk - inj.lhcbunch) +  "</td><td>"
                + ( inj.lhcbunch + inj.getLength() - 1) 
                + ' (' + ( 3442 - (inj.lhcbunch + inj.getLength() - 1) ) + ')'
                + '</td><td>' + ( inj.lhcbunch + inj.getLength() - 1 - lhc.agk )
                + '</td><td>' + waste + '</td>';
        }
    }

    this.injhash_b1 = injhash_b1;

    spacstr += "</tr>\n";
    colstr  += '<th>last bunch (av.)</th><th>AGK-use</th><th>usable free bx</th></tr>' + spacstr + "<tr><th>B2</th><td></td>";

    var sp2 = 0;
    waste = 0;
    if ( lhc.beam2.injections.length > 0 ) {
        sp2 = lhc.beam2.injections[0].lhcbunch;
        waste = lhc.beam2.injections[0].lhcbunch;
    }
    var spacstr = '<tr><td>empty</td><td>' + sp2 + '</td>';

    //var injedstr_b2 = "";
    var injhash_b2 = {};
    for (var i=0; i<lhc.beam2.injections.length; i++) {

        var inj = lhc.beam2.injections[i];

        maxlenb2 = Math.max( maxlenb2, inj.getLength() );

        var noba = inj.batches.length;
        var babu = inj.noBunches / noba;
        var key = noba + " x " + babu;

        if (! (key in injhash_b2) ) {
            injhash_b2[key] = {'positions' : [],
                               'nbatch'    : noba,
                               'nbunch'    : babu,
                               'batch_sp'  : inj.batchSpacing,
                               'bunch_sp'  : inj.batches[0].bunch_spacing };
        }

        colstr += '<td>' + inj.lhcbunch+"</td>";
        injhash_b2[key]['positions'].push( inj.lhcbunch );

        if ( i < lhc.beam2.injections.length-1 ) {
            var space = lhc.beam2.injections[i+1].lhcbunch - inj.lhcbunch
                - inj.getLength() + 1;
            spacstr +=  '<td>' + space + '</td>';
            waste += space - lhc.injSpacing;
        } else {
            waste += lhc.agk - inj.lhcbunch;
            spacstr += "<td>" + (lhc.agk - inj.lhcbunch) +  "</td><td>"
                + (inj.lhcbunch + inj.getLength() - 1)
                + ' (' + ( 3442 - (inj.lhcbunch + inj.getLength() - 1) ) + ')'
                + '</td><td>' + ( inj.lhcbunch + inj.getLength() - 1 - lhc.agk )
                + '</td><td>' + waste + '</td>';
        }
    }

    maxilen = Math.max( maxlenb1, maxlenb2 ) - 1;
    //console.log( "max injection length", maxilen, "agk", lhc.agk );
    var agk_opt = 35640 - 1200 - 20 - 10*maxilen + 1;
    $('label#optagk').html( agk_opt );
    if (10*lhc.agk > agk_opt ) {
        $('label#agkwarn').html( "The current AGK is incompatible with the maximal injection length" );
    } else {
        $('label#agkwarn').html("");
    }
    this.injhash_b2 = injhash_b2;
    
    colstr += "</tr>" + spacstr + "</tbody></table></td></tr></tbody></table></tbody></table>\n";
    colstr += "<p>";


    colstr += "<h3>Beam 1 Injection editor strings</h3>"
    for (var k in injhash_b1) {
        colstr += k + " : "
        for (var b in injhash_b1[k]['positions']) {
            colstr += injhash_b1[k]['positions'][b]*10+1 + ",";
        }
        colstr = colstr.slice(0,-1);
        colstr += "<br>";
    }
    colstr += "<br>"

    colstr += "<h3>Beam 2 Injection editor strings</h3>"

    for (var k in injhash_b2) {
        colstr += k + " : "
        for (var b in injhash_b2[k]['positions']) {
            colstr += injhash_b2[k]['positions'][b]*10+1 + ",";
        }
        colstr = colstr.slice(0,-1);
        colstr += "<br>";
    }
    colstr += "</p>";

    el.html(colstr);
    

    // long ranges
    var lr = res['LongRanges'];
    lrstr = '<table id="longrange">';
    lrstr += '<tr><th style="border-bottom: 1px solid gray">long ranges<br>exp / dist[m]</th>';
    var length = lr['IP15']['B1'].length
    var lrmax = (length-1)/2;
    var step = 3.7474;
    for (var i = -lrmax; i <= lrmax; i++) {
        lrstr += '<td style="border-bottom: 1px solid gray">' + (i * step).toFixed(1) + '</td>';
    }
    lrstr += "</tr>"
    for (exp in lr) {
        lrstr += "<tr><th>"+exp+"</th>";
        for (var i = -lrmax; i <= lrmax; i++) {
            var ins = "";
            if ( lr[exp]['B1'][i+lrmax] != 0 ) {
                ins = lr[exp]['B1'][i+lrmax];
            }
            lrstr += '<td>' + ins + '</td>';
        }
        lrstr += '<tr>';
    }
    lrstr += '</table>';

    lrel.html( lrstr );

}

