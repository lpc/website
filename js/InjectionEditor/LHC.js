LHC = function() {
    //debug("lhc constr");
    this.AGK = 31181; // could be 32822 LBDS AGK or 32842 (MKI AGK)
    this.agk = 3118;
    this.buckets = 35640;
    this.injSpacing = 950/25;
    this.beam1 = new Beam(1);
    this.beam2 = new Beam(2);
    this.bpi = 0;
    this.batchSpacing = 0;
    this.schemeName = "";
    this.collisions = {};

}

LHC.prototype.checkEmptyWitnessZone = function() {
    return this.beam1.checkEmptyWitnessZone() || this.beam2.checkEmptyWitnessZone();
}

LHC.prototype.getBeam = function( beamNo ) {
    if (beamNo == 1)
        return this.beam1;
    if (beamNo == 2)
        return this.beam2;
    return null;
}

LHC.prototype.getOtherBeam = function( beamNo ) {
    if (beamNo == 1)
        return this.beam2;
    if (beamNo == 2)
        return this.beam1;
    return null;
}

LHC.prototype.setInjectionSpacing = function( spc ) {
    this.injSpacing = parseInt(spc/25);
    this.beam1.injSpacing = this.injSpacing;
    this.beam2.injSpacing = this.injSpacing;
}

LHC.prototype.setAGK = function( agk ) {
    this.AGK = agk;
    this.agk = parseInt( this.AGK / 10 );
    this.beam1.agk = this.agk;
    this.beam2.agk = this.agk;
}

LHC.prototype.moveSelection = function( delta ) {
    this.beam1.moveSelection( delta );
    this.beam2.moveSelection( delta );
}

LHC.prototype.suggestName = function() {
    var b1 = this.beam1;
    var b2 = this.beam2;
    var name = "Multi_";
    this.bpi = 0;
    this.batchSpacing = 1000000;
    for (var i=b1.injections.length-1; i>=0; i--) {
        var inj = b1.injections[i];
        var ba = inj.batches[0];
        //console.log(inj);
        if (inj.batches.length > 1) {            
            if ( (inj.batchSpacing + 1) * 25 < this.batchSpacing ) {
                this.batchSpacing = (inj.batchSpacing + 1) * 25;                
            }
        }
        if (ba.bunches > 1)
            name = ba.bunch_spacing + "ns_";
        if (inj.noBunches > this.bpi)
            this.bpi = inj.noBunches;
    }

    var basptxt = ""
    if ( this.batchSpacing < 1000000 ) {
        basptxt = "_bs" + this.batchSpacing + "ns";
    }
    //console.log( "basptxt", basptxt )
    name += this.collisions['NB1'] + "b_" + this.collisions["ATLAS/CMS"] + "_" +
        this.collisions["ALICE"] + "_" + this.collisions["LHCb"] + "_" + this.bpi + "bpi_" +
        b1.injections.length + "inj_" + this.injSpacing*25 + "ns" + basptxt;
    
    this.schemeName = name;
}


// sets this.collisions adn this.beam1.bunchClasses this.beam2.bunchClasses
LHC.prototype.countCollisions = function() {
    var nlongranges = 25; // 3.75m per lr, corresponds to 93.75m 
    nlongranges += 1;
    var res = { 'ATLAS/CMS' : 0,
                'LHCb'      : 0,
                'ALICE'     : 0,
                'NC1'       : 0,
                'NC2'       : 0,
                'NB1'       : 0,
                'NB2'       : 0, 
                'B1_classes': [0,0,0,0,0,0,0,0],
                'B2_classes': [0,0,0,0,0,0,0,0],
                'LongRanges': {'IP15' : {'B1' : new Array( 2*nlongranges-1 ).fill(0),
                                         'B2' : new Array( 2*nlongranges-1 ).fill(0) },
                               'IP2' : {'B1' : new Array( 2*nlongranges-1 ).fill(0),
                                        'B2' : new Array( 2*nlongranges-1 ).fill(0) },
                               'IP8' : {'B1' : new Array( 2*nlongranges-1 ).fill(0),
                                        'B2' : new Array( 2*nlongranges-1 ).fill(0) }
                              }
              };


    var collarrs = { 'ATLASCMS' : [[],[]],
                     'LHCb' : [[],[]],
                     'ALICE' : [[],[]] }
    
    var b1 = this.beam1.bunches;
    this.beam1.bunchClasses = new Array( b1.length )
    // makes a deep copy... we modify the array here
    var b2 = this.beam2.bunches.slice(0);
    this.beam2.bunchClasses = new Array( b2.length )
    bucl1 = this.beam1.bunchClasses;
    bucl2 = this.beam2.bunchClasses;


    //debug( "lengths " + b1.length +" "+ b2.length);
    for ( var i=0; i<b1.length; i++ ) {
        bucl1[i] = -1;
    }
    for ( var i=0; i<b2.length; i++ ) {
        bucl2[i] = -1;
    }
        
    
    for (var ix = 0; ix<b1.length; ix++ ) {

        if (b1[ix] == 0) {
            continue;
        }
        res['NB1']++;
        bucl1[ix] = 0;
        nc = 1;  
        var cat1 = 0;
        var cat2 = 0;
        if ( b2[ix] > 0 ) {
            res['ATLAS/CMS'] += 1;
            collarrs.ATLASCMS[0].push(ix*10+1);
            nc = 0;
            b2[ix]++;
            cat1 |= 1;
            bucl1[ix] |= 1;
        }
        // IP1/5 long ranges
        for ( var il = -nlongranges+1; il < nlongranges; il++ ) {

            // IP1/5 long ranges
            var itb = (ix + il) % 3564;
            if (b2[itb] > 0) {
                res['LongRanges']['IP15']['B1'][il+nlongranges-1] += 1;
            }

            // IP8 long ranges
            itb = (ix - 894 + il);
            if ( itb < 0 ) 
                itb += 3564;
            if (b2[itb] > 0) {
                res['LongRanges']['IP8']['B1'][il+nlongranges-1] += 1;
            }

            // IP2 long ranges
            itb = (ix + 891 + il) % 3564;
            if (b2[itb] > 0) {
                res['LongRanges']['IP2']['B1'][il+nlongranges-1] += 1;
            }
        }
        
        if ( b2[(ix+891) % 3564 ] > 0 ) {
            res['ALICE'] += 1;
            collarrs.ALICE[0].push( ix*10+1 );
            nc = 0;
            b2[(ix+891) % 3564 ]++;
            cat1 |= 2;
            bucl1[ix] |= 2;
        }
        var iy = ix-894;
        iy = iy < 0 ? iy + 3564 : iy;

        if (b2[iy] > 0) {
            res['LHCb'] += 1;
            collarrs.LHCb[1].push(iy*10+1);
            nc = 0;
            b2[iy]++;
            cat1 |= 4;
            bucl1[ix] |= 4;
        }
        res['NC1'] += nc;
        res['B1_classes'][cat1]++; 
    }

    //console.log("Collision arrays filled");
    this.collisionArrays = collarrs;
    
    for ( var ix = 0; ix < b2.length; ix++ ) {
        if ( b2[ix] > 0 ) {
            res['NB2']++;
        }
        if ( b2[ix] == 1 ) {
            res['NC2']++;
        }

        if ( b2[ix] > 0 ) {
            for ( var il = -nlongranges+1; il < nlongranges; il++ ) {

                // check long ranges in IP1/5
                var itb = (ix - il);
                itb = itb < 0 ? itb + 3564 : itb;
                if (b1[itb] > 0) {
                    res['LongRanges']['IP15']['B2'][il+nlongranges-1] += 1;
                }
                
                // check long ranges in IP8
                itb = (ix + 894 - il) % 3564;
                if ( b1[itb] > 0 ) {
                    res['LongRanges']['IP8']['B2'][il+nlongranges-1] += 1;
                }

                // check long ranges in IP2
                itb = ix - 891 - il;
                if ( itb < 0 ) 
                    itb += 3564;
                if ( b1[itb] > 0 ) {
                    res['LongRanges']['IP2']['B2'][il+nlongranges-1] += 1;
                }

            }
        }
    }
    
    var b2o = this.beam2.bunches;
    for  ( var ix = 0; ix < b2o.length; ix++ ) {
        if (b2o[ix] == 0) {
            continue;
        }
        bucl2[ix] = 0;
        var cat2 = 0;
        if ( b1[ix] > 0 ) {
            cat2 |= 1;
            bucl2[ix] |= 1;
        }
        
        var iy = ix - 891;
        iy = iy < 0 ? iy + 3564 : iy;
        
        if (b1[iy] > 0) {
            cat2 |= 2;
            bucl2[ix] |= 2;
        }

        if ( b1[(ix+894) % 3564 ] > 0 ) {
            cat2 |= 4;
            bucl2[ix] |= 4;
        }

        res['B2_classes'][cat2]++; 

    }

    this.collisions = res;

    //console.log( 'collisions', res );

    return res;
}
