debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
warning = function( txt ) {
    $('div#warning').html( txt );
}
error = function( txt ) {
    $('div#error').html( txt  );
}

CKEDITOR.disableAutoInline = true;
var editor = undefined;

runEditor = function() {
    setTimeout( checkEditor, 3000 );
}

$('div#pagetext').keydown( function() {
    $('div#status').html( "modified" );
});

addToList = function() {
    var indicourl = $('input#indicourl').val();
    var date = $('input#date').val();
    var suffix = $('input#suffix').val();
    var shortdesc = $('input#shortdesc').val();
    if ( date == "" ) {
        error("You must enter a date for the meeting");
        return;
    }
    if ( shortdesc == "" ) {
        error( "You must enter a short description for the meeting");
        return;
    }

    var data = { 'indicourl' : indicourl,
                 'date' : date,
                 'sufix' : suffix,
                 'shortdesc' : shortdesc,
                 'action' : "addToList"
               };

    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    $.post( url, data, function( data ) {
	//console.log( data );
        $('div#status').html(data.status);
	var url = "editPage.py?action=loadpage&page=lpc-meetings.htm"
	var win = window.open( url, '_blank');
    });
        
}

collectData = function( action ) {
    if (editor) {
        var text = editor.getData(false);
    } else {
        var text = "";
    }
    var date = $('input#date').val();
    var suffix = $('input#suffix').val();
    var indicourl = $('input#indicourl').val();
    var shortdesc = $('input#shortdesc').val();
    var purpose = $('textarea#purpose').val();
    if (date == "" || shortdesc == "" || purpose == "" || text == "") {
        error( "You must have filled out all fields to save (...including some text in the editor...). The suffix or indicourl field may be empty." );
        return false;
    }
    var record = { 'text' : text,
                   'date' : date,
                   'suffix' : suffix,
                   'indicourl' : indicourl,
                   'shortdesc' : shortdesc,
                   'purpose' : purpose,
                   'action' : action};
    return record;
}

checkEditor = function() {
    if ( editor.checkDirty() ) {
        data = collectData( 'save' );
        if ( ! data ) {
            setTimeout( checkEditor, 3000 );
            return;
        }
        var url = window.location.protocol + '//' + window.location.host;
        var path = window.location.pathname;
        path = path.substring(0,path.lastIndexOf('/'));
        url += path + '/minutesHandler.py';
        //debug("posting to " + url );
        $.post( url, data, function( data ) {
            //debug( data );
            //if ('debug' in data) { debug( data.debug ); }
            $('div#status').html(data.status);
        } );
        editor.resetDirty();
    }

    setTimeout( checkEditor, 3000 );

    return;
}

clearall =  function(){
    warning("");
    error("");
    $('input#date').val("");
    $('input#suffix').val("");
    $('input#indicourl').val("");
    $('input#shortdesc').val("");
    $('textarea#purpose').val("");
    $('td#devlink').val("");

}
newSummary = function() {
    clearall();
    if ( editor == undefined ) {
        editor = CKEDITOR.inline( 'pagetext');
        editor.on('key', function() {
            $('div#status').html("changed");
        });
        runEditor( );
    }
}

release = function( ) {
    // make sure the current editor data is saved
    var status = $('div#status').html();
    if ( status == "problem" )
        return;
    if (status == "changed" ) {
        myI = setTimeout( function() { release( "release" ); }, 1000 );
        warning("Waiting for summary to be saved...");
        return;
    }
    warning("");
    data = collectData( "release" );    
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    $.post( url, data, function( data ) {
        //debug(data);
        $('div#status').html( data.status );
        if ( 'link' in data ) {
            $('td#devlink').html( '<a href="' + data.link + '" target="_blank">Link to final minutes in git-source/lpc-minutes</a>')
        } else {
            $('td#devlink').html('');
        }
        if ('error' in data) { error( data.error ); };
        if ('debug' in data) { debug( data.debug ); }
    })
    return
}
    
publishSummary = function( action ) {
    // make sure the current editor data is saved
    var status = $('div#status').html();
    if (status != "saved" && status != "loaded") {
        myI = setTimeout( function() { publishSummary( action ); }, 1000 );
        warning("Waiting for summary to be saved...");
        return;
    }
    warning("");
    data = collectData( action );    
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    $.post( url, data, function( data ) {
        //if ('debug' in data) { debug( data.debug ); }
        console.log( data );
        $('div#status').html( data.status );
        if ( 'link' in data ) {
            $('td#devlink').html( '<a href="' + data.link + '" target="_blank">Link to final minutes in git-source/lpc-minutes</a>')
        } else {
            $('td#devlink').html('');
        }
        if ('error' in data) { error( data.error ); };
    })
    return
}
            
loadSummary = function() {
    data = { 'text' : "",
             'action' : 'load' };
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    //debug("load from " + url );
    $.post( url, data, function( data ) {
        //debug( data );
        $('div#pagetext').html( data.text );
        $('input#date').val( data.date );
        $('input#suffix').val( data.suffix );
        $('input#indicourl').val( data.indicourl );
        $('input#shortdesc').val( data.shortdesc );
        $('textarea#purpose').val( data.purpose );
        if( data.link != "" ) {
            $('td#devlink').html( '<a href="' + data.link + '" target="_blank">Link to final minutes in git-source/lpc-minutes</a>');
        } else {
            $('td#devlink').html( '' );
        }

        if ("debug" in data) { debug( data.debug ); }
        if ("error" in data) { error( data.error ); }

        if ( editor == undefined ) {
            editor = CKEDITOR.inline( 'pagetext');
            editor.addContentsCss( '' );
            editor.resetDirty();
            editor.on('key', function() {
                $('div#status').html("changed");
            });
            
            runEditor( );
        }
        $('div#status').html(data.status);
    } );
    
}

loadPreviousSummary = function() {
    var sumname = $('select#summaryName').val();
    if ( sumname == "" ) {
        return;
    }
    data = { 'text' : "",
             'action' : 'loadPrevious',
             'summaryName' : sumname };
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    
    $.post( url, data, function( data ) {
        //debug( data );
        $('div#pagetext').html( data.text );
        $('input#date').val( data.date );
        $('input#suffix').val( data.suffix );
        $('input#indicourl').val( data.indicourl );
        $('input#shortdesc').val( data.shortdesc );
        $('textarea#purpose').val( data.purpose );
        if (data.link != "" ) {
            $('td#devlink').html( '<a href="' + data.link + '" target="_blank">Link to final minutes in git-source/lpc-minutes</a>');
        } else {
            $('td#devlink').html( '' );
        }

        if ("debug" in data) {
            debug( data.debug );
        }
        if ( editor == undefined ) {
            editor = CKEDITOR.inline( 'pagetext');
            editor.on('key', function() {
                $('div#status').html("changed");
            });
            editor.resetDirty();
        
            runEditor( );
        } else {
            editor.resetDirty();
        } 
        $('div#status').html(data.status);
    } );
    
}
