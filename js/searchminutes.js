var replyPending = 0;
var searchRequest = 0;
var searchtext = "";

debug = function( txt ) {
    $('div#debug').append( txt + "<br>");
}

error = function( txt ) {
    $('div#error').append( txt + "<br>");
}

clear_error = function() {
    $('div#error').html("");
}

txtenter = function( evt ) {
    var txt = $('input#searchtext').val();
    if (txt.length >= 2) {
	searchRequest = 1;
	searchtext = txt;
	$('#numberofmatches').html('searching for <i>"' + txt + '"</i> .');
	$('#numberofmatches').show("blind");
    } else {
	$('#numberofmatches').hide("blind");
	$('table#searchtable').hide("blind");
    }
}

postIt = function( baseurl ) {
    if ( replyPending == 1 ) {
	$('#numberofmatches').append(".");
	return;
    }
    if ( searchRequest == 0 ) {
	return;
    }

    replyPending = 1;
    searchRequest = 0;
    postSearch( { searchstring: searchtext }, baseurl );
}

postSearch = function( data, baseurl ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    //debug( JSON.stringify(data) );
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/' + baseurl;
    //debug( JSON.stringify(data));
    //debug( url );

    $.ajax( {type: "GET",
	     url: url,
	     data: data,
//	     data: $('form#search').serialize(),
             timeout: 10000,
	     dataType: "json",
	     success: function(reply,status) {
		 if ( status != "success" ) {
		     error( "Error communicating with server: " + status);
		     return;
		 } else {
		     //debug( "reply came");
		     if ( reply.debug ) debug( reply.debug );
		     if ( reply.info  ) debug( reply.info );
		     if ( 'error' in reply ) {
			 $('#numberofmatches').html(reply.error);
			 $('#numberofmatches').show("blind");
			 
			 //clear_error();
			 //error( reply.error );
			 replyPending = 0;
		     } else {
			 clear_error();
		     }
		     if ( reply.data ) {
			 handleSearchReply( reply.data );
		     }
		 }
	     },
	    }
	  );
};

handleSearchReply = function( data ) {

    var txt = $('input#searchtext').val(); 
    var plural = "";
    if (data.matches > 1)
        plural = "s";
    if ( txt.length >= 2) 
	$('#numberofmatches').html(data.matches + ' report' + plural + ' found when searching for <i>"' + data.querystring + '"</i>.');
	$('#numberofmatches').show("blind");

    if ( data.matches < 1 )
	$('table#searchtable').hide("blind");
    
    $('#searchtable tbody').html("")

    var dates = Object.keys(data.items).sort().reverse();
    for ( var i=0; i<dates.length; i++ ) {
	var date = dates[i];
	item = data.items[date];
	$('table#searchtable tbody').append( '<tr><td><a href="' + item.link + '" target="_blank">' + date + "</a></td><td>" + data.items[date].sys + "</td></tr>" );
	
    }


    if ( data.matches > 0 && txt.length >= 2) 
	$('table#searchtable').show("blind");


    replyPending = 0;
}

