var curData = {};
var xrp = {};

recalcResults = function() {
    var emitt = $('input#norm_emittance').val();
    var ip = $('select#ipsel').val();
    var data = { 'action' : 'calcResult',
                 'dir' : $('select#dirsel').val(),
                 'ip' : ip,
                 'emittance' : emitt };
    //console.log( data );
    postIt( data, "handleOptics.py" );
}
    
changeDir = function() {
    var dir = $('select#dirsel').val();
    // populate the file sel
    var filestr = "<option></option>"
    //debug( dir );
    var arr = fileh[dir]['files'];
    for ( var i=0; i < arr.length; i++ ) {
	filestr += '<option value="' + arr[i] + '">' + arr[i].split(/[\\/]/).pop() + '</option>';
    }
    $('div#description').html( fileh[dir]['description'] );
    //console.log( filestr );
    $('select#opticsfilesel').html( filestr );
}

chooseOptics = function() {
    filename = $('select#opticsfilesel').val();

    if ( ! filename ) 
        return;

    var data = {'action' : 'chooseFile', 
                'filename' : filename };

    postIt( data, "handleOptics.py" );

}


postIt = function( data, baseurl ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/' + baseurl;
    //debug( url );
    $.post( { 
        url: url,
        data: data
    }).done( function(reply,status) {
        //debug( "reply");
        //console.log( reply, status );
	if ( status != "success" ) {
	    error( "Error communicating with server: " + status);
	    return;
	} else {
            //debug( "reply came");
            if ( typeof reply === "string" )
                debug( reply );
	    //debug( "reply came, " + reply.action);
	    if ( reply.debug ) debug( reply.debug );
	    if ( reply.info  ) debug( reply.info );
	    if ( reply.error ) error(reply.error);
	    else clerror();
            if ( ! 'action' in reply ) {
                debug( "no action found");
                debug( reply );
            }
            if ( reply['action'] == "chooseFile" ) {
                //console.log( reply.data );
                if ( reply.type == "tfs" ) {
                    populateVars( reply.data );
                    xrp = reply.xrp;
                }
                if ( reply.type == "json" ) {
                    curData = reply.data;
                    plotResult();
                }
            } else if( reply['action'] == 'calcResult' ) {
                //console.log( "calcresult", data );
                curData = reply.data;
                xrp = reply.xrp;
                plotResult();
            }
        }
    });
    
}

populateVars = function( data ) {
    curData = data;
    var cols = curData.beam1.columns;
    $('select#varsel').html( "" );
    for ( var i = 0; i< cols.length; i++ ) {
        $('select#varsel').append( '<option value="'+ cols[i] +'">'+cols[i]+'</option>' );
    }
}

plotvar = function() {

    var varname = $('select#varsel').val();
    var soff = 0;
    var soff2 = 0;
    var b1cols = curData.beam1.columndata;
    var b2cols = curData.beam2.columndata;
    var iptxt = curData.ip.toUpperCase();


    if ( iptxt ) {
        var ioff = -1;
        for ( ioff = 0; ioff < b1cols['NAME'].length; ioff++ ) {
            //console.log ("compare ", ":"+b1cols['NAME'][ioff]+":", ":"+iptxt+":");
            if ( b1cols['NAME'][ioff] == iptxt ) {
                break
            }
        }
        if (ioff == b1cols['NAME'].length)
            error("Did not find offset for ip " + iptxt );
        else
            soff = b1cols['S'][ioff]

        // just to check:
        var ioff2 = -1;
        for ( ioff2 = 0; ioff2 < b2cols['NAME'].length; ioff2++ ) {
            if ( b2cols['NAME'][ioff2] == iptxt )
                break
        }
        if (ioff2 != ioff)
            error("ip offset for beam1 and beam2 are not the same:  " + ioff + " <--> " + ioff2 );
        else
            soff2 = b2cols['S'][ioff2]
        if ( soff != soff2 ) 
            console.log( "The offsets of the IP in s are not the same for both beams: beam 1 = " + soff + "  beam 2 = " + soff2 );
    } else {
        console.log("no particular ip");
    }
    var S1 = curData.beam1.columndata['S'];
    var y1 = curData.beam1.columndata[varname];
    pdata1 = [];
    for (var i=0; i<S1.length; i++) {
        pdata1.push( [ S1[i] - soff, y1[i] ] );
    }

    var xrplines = [];
    for (beam in xrp) {
        for (var ix = 0; ix < xrp[beam].length; ix++) {
            console.log( xrp[beam][ix] );
            xrplines.push( { 'color' : 'black',
                             'width' : 1,
                             'value' : xrp[beam][ix].S - soff,
                             'label' : { 'text' : xrp[beam][ix].name } 
                           } );
        }
    }


    var S2 = curData.beam2.columndata['S'];
    var y2 = curData.beam2.columndata[varname];
    pdata2 = [];
    for (var i=0; i<S2.length; i++) {
        pdata2.push( [ S2[i] - soff2, y2[i] ] );
    }

    // calculate the crossing angle:
    var ds = S1[ioff+2] - S1[ioff-2];
    var dx = b1cols['X'][ioff+2] - b1cols['X'][ioff-2];
    var dy = b1cols['Y'][ioff+2] - b1cols['Y'][ioff-2];
    console.log( "beam 1 crossing ds dx dy", ds,dx,dy );
    var halfx_x = Math.atan(dx/ds)*1e6;
    var halfx_y = Math.atan(dy/ds)*1e6;
    console.log( "half horizontal crossing angle: ", halfx_x, "ur" );
    console.log( "half vertical   crossing angle: ", halfx_y, "ur" );

    var theChart = undefined;
    $(function() {
        theChart = $('div#plotarea_1').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'xy'
            },
            title: {
                text : "plot of optics",
                style: {
                    color: '#000080',
                    'font-size': "150%"
                }
            },
            xAxis: {
		title : {
		    text : 'S'
		},
                plotLines : xrplines
            },
            yAxis: [
                {
                    title : {
                        text : "???",
                        style: {
                            'font-size':"130%",
                            color : 'black'
                        }
                    }
                },
                {
                    title : {
                        text: "???",
                        style: {
                            "font-size" : "130%",
                            "color" : 'black'
                        }
                    },
                    opposite: true
                }
            ],
            series: [
                {
                    name: 'Beam 1 data',
                    data: pdata1,
                    color: '#0000FF'
                },
                {
                    name: 'Beam 2 data',
                    data: pdata2,
                    color: '#FF0000'
                },
            ]
        });
    });
};

plotResult = function() {

    var pdata = curData;

    var beam;
    var xrplines = [];
    for (beam in xrp) {
        for (var ix = 0; ix < xrp[beam].length; ix++) {
            console.log( xrp[beam][ix] );
            xrplines.push( { 'color' : 'black',
                             'width' : 1,
                             'value' : xrp[beam][ix].S,
                             'label' : { 'text' : xrp[beam][ix].name } 
                           } );
        }
    }

    var resultChart = undefined;
    $(function() {
        resultChart = $('div#plotarea_2').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'xy'
            },
//            title: {
//                text : "plot of optics",
//                style: {
//                    color: '#000080',
//                    'font-size': "150%"
//                }
//            },
            xAxis: {
		title : {
		    text : 'S'
		},
                plotLines: xrplines
            },
            yAxis: [
                {
                    title : {
                        text : "Distance Beam1 Beam2 [m]",
                        style: {
                            'font-size':"130%",
                            color : 'black'
                        }
                    }
                },
                {
                    title : {
                        text: "Sigma [m]",
                        style: {
                            "font-size" : "130%",
                            "color" : 'black'
                        }
                    },
                    opposite: true
                },
                {
                    title : {
                        text: "Distance in Sigma-X",
                        style: {
                            "font-size" : "130%",
                            "color" : 'black'
                        }
                    },
                    opposite: true
                }
            ],
            series: pdata
        });
    });
};
