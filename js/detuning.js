var elementary_charge = 1.602176621e-19
var theChart = {}
var dtdata = {}

domega_opt = function( params, nbunch, Ib ) {
    var domega = -0.5 * 2.0 * Math.PI * params.f_RF * params.RoverQ * Ib / params.V0
    return domega
}

create_injection = function( bunches, batches, batch_sp ) {
    var arr = [];
    //console.log( "crea injec " , bunches, batches, batch_sp );
    for ( var i=0; i<batches; i++ ) {
        //console.log( "batch " ,  i);
        for ( var b=0; b<bunches; b++ ) {
            arr.push(1);
        }
        if ( i < batches-1 )
            for ( var e=0; e<batch_sp; e++ )
                arr.push(0);
    }
    //console.log( "inj ", arr);
    return arr;
}

create_beam = function( population ) {
    var arr = [];
    for ( var i=0; i<3564; i++ )
        arr.push(0);

    for ( var i=0; i<population.length; i++ ) {
        var kind = population[i];
        //console.log( "kind ", kind);
        var inj = kind.injection
        //console.log( "injection " , inj );
        for ( var ipos=0; ipos<kind.positions.length; ipos++ ) {
            var ix = 0;
            for (var cb=kind.positions[ipos]; cb < kind.positions[ipos]+inj.length; cb++) {
                arr[cb] = inj[ix];
                ix += 1;
            }
        }
    }
    //console.log( "beam ", arr);
    return arr;
}

numberOfBunches = function(beam){
    var nb = 0;
    //console.log( beam );
    for ( var i=0; i<beam.length; i++ )
        if (beam[i] == 1)
            nb++;
    //console.log( "beam length " ,nb );
    return nb;
}

getdelta = function( nb, beam, nbunch, params, domega, Ib, ibt ) {
    var dphi = 0;
    if (beam[nb] == 0 ) {
        dphi = domega * 25.0e-9;
    } else {
        dphi = domega * ( 25.0e-9 * ( Ib - ibt)/Ib); 
        //dphi = domega * ( ( 25.0e-9 - params.b_len ) - ( itemp - Ib ) / Ib * params.b_len )        
    }
    return dphi;
}

calcPhase = function( beam, params, collisions ) {
    var resvec = [];
    var timevec = [];
    var integ = 0.0;
    var nbunch = numberOfBunches( beam );
    var len = params.b_len;
    var frf = params.f_RF;
    var t1 = frf*Math.PI*len;
    var fbp = (-3.0*t1*Math.cos(t1) + 3*Math.sin(t1))/(t1*t1*t1);
    //console.log( "fbp ", fbp )
    //var fbp = 0.85;
    var ibt = 2.0 * fbp * params.ppb * elementary_charge / 25.0e-9;
    var Ib = nbunch * ibt / 3564.0;
    var domega = domega_opt( params, nbunch, Ib );
    //var Ib = params.ppb * nbunch * params.f_rev * elementary_charge;
    //console.log( "nbu, domega, Ib ", nbunch, domega, Ib );
    //console.log( "params ", params );
    var min = undefined;
    var max = undefined;
    var sum = 0;
    for( ibx = 0; ibx < 3564; ibx++ ) {
        var dphi = getdelta( ibx, beam, nbunch, params, domega, Ib, ibt );
        integ += dphi;
        resvec.push( integ );
	// positive phase means it comes earlier : negative time
	// negative phase means it comes later : positive time
        time = -1.0/params.f_RF/2.0/Math.PI * integ * 1.0e12;
        timevec.push(time);
	sum += time;
        if (min == undefined) {
            min = time;
            max = time;
        } else {
            if (time < min)
                min = time
            if (time > max)
                max = time
        }
    }
    var mean = sum / 3564.0;
    deltamax = max - min;
    var yshift = -mean;
    for (var ix in timevec)
        timevec[ix] += yshift;

    return {'phaseshift' : timevec,
            'maxshift'   : deltamax };
}

calcZoffset = function( b1, b2) {
    var ov = [];
    for (var ix = 0; ix<b1.length; ix++) {
        var delta = b2[ix]-b1[ix];
        var offset = delta / 2.0 * 2.99792458e-2; // delta is in picoseconds; result in cm
        ov.push( offset );
    }
    return ov;
}


makeChart = function( id, title, b1phase, b2phase, phasediff, displacement, collisions, xtitle ) {

        
    if ( xtitle == undefined ) {
	xtitle = "Bunch crossing id (0 ... 3564)";
    }

    // find min and max of phases for collisions
    var collarr = [];
    for ( var ix=0; ix<3564; ix++ ) 
	collarr.push(0.0);
    for ( var ix=0; ix<collisions.length; ix++ )
	collarr[(collisions[ix]-1)/10] = 1;
    
    var b1min = 9e19;
    var b1max = -9e19;
    var b2min = 9e19;
    var b2max = -9e19;
    var dispmax = -9e19;
    var dispmin = 9e19;
    var phasediffmin = 9e19;
    var phasediffmax = -9e19;
    for ( var ix=0; ix<3564; ix++ ) {
	if ( collarr[ix] == 0 )
	    continue;
        if ( displacement[ix] < dispmin )
            dispmin = displacement[ix];
        if ( displacement[ix] > dispmax )
            dispmax = displacement[ix];
	if ( b1phase.phaseshift[ix]<b1min )
	    b1min = b1phase.phaseshift[ix];
	if ( b2phase.phaseshift[ix]<b2min )
	    b2min = b2phase.phaseshift[ix];
	if ( phasediff.phaseshift[ix]<phasediffmin )
	    phasediffmin = phasediff.phaseshift[ix];
	if ( b1phase.phaseshift[ix]>b1max )
	    b1max = b1phase.phaseshift[ix];
	if ( b2phase.phaseshift[ix]>b2max )
	    b2max = b2phase.phaseshift[ix];
	if ( phasediff.phaseshift[ix]>phasediffmax )
	    phasediffmax = phasediff.phaseshift[ix];
    }
    b1phase['maxshift_colliding'] = b1max - b1min;
    b2phase['maxshift_colliding'] = b2max - b2min;
//    b1phase['minshift_colliding'] = b1min;
//    b2phase['minshift_colliding'] = b2min;
    phasediff['min_colliding'] = phasediffmin;
    phasediff['max_colliding'] = phasediffmax;

    //console.log( 'collisions atl', collisions)
    var bands = calcBands( collisions, '#c0c0c0' );
    //console.log( 'bands', bands );
    
    plotid = "div#"+id+"_plot";
    commentid = "div#"+id+"_comment";

    $(commentid).html( "<table class=\"phaseinfo\"><tr><td>Peak to peak delay difference beam 1 for colliding bunches</td><td>:</td><td>" + b1phase.maxshift_colliding.toPrecision(3) + "ps</td></tr><tr><td>Peak to peak delay difference beam 2 for colliding bunches</td><td>:</td><td>" + b2phase.maxshift_colliding.toPrecision(3) + "ps</td></tr><tr><td>Minimal and maximal collision time shift for colliding bunches</td><td>:</td><td>" + phasediff.min_colliding.toPrecision(3) + "ps &nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;" + phasediff.max_colliding.toPrecision(3) + "ps &nbsp;&nbsp : &nbsp;&nbsp;&Delta; = " + (phasediff.max_colliding - phasediff.min_colliding).toPrecision(3) + "ps</td></tr><tr><td>IP displacement in z for colliding bunches</td><td>:</td><td>" + dispmin.toPrecision(3) + "cm&nbsp;&nbsp;to&nbsp;&nbsp;" + dispmax.toPrecision(3) + "cm&nbsp;&nbsp;:&nbsp;&nbsp;&Delta; = " + (dispmax - dispmin).toPrecision(3) + "cm</td></tr></table>" );
    
    var theChart = undefined;
    $(function() {
        theChart = $(plotid).highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'x'
            },
            title: {
                text : title,
                style: {
                    color: '#000080',
                    'font-size': "150%"
                }
            },
            xAxis: {
                plotBands: bands,
		title : {
		    text : xtitle
		}
            },
            yAxis: [
                {
                    title : {
                        text : "[ps]",
                        style: {
                            'font-size':"130%",
                            color : 'black'
                        }
                    }
                },
                {
                    title : {
                        text: "[cm]",
                        style: {
                            "font-size" : "130%",
                            "color" : 'black'
                        }
                    },
                    opposite: true
                }
            ],
            series: [
                {
                    name: 'Beam 1 delay to LHC clock',
                    data: b1phase.phaseshift,
                    color: '#0000FF'
                },
                {
                    name: 'Beam 2 delay to LHC clock',
                    data: b2phase.phaseshift,
                    color: '#FF0000'
                },
                {
                    name: 'Collision time wrt LHC clock',
                    data: phasediff.phaseshift,
                    color: '#FF8000'
                },
                {
                    name: 'Z-displacement of IP',
                    yAxis: 1,
                    data: displacement,
                    color: '#208020'
                }
            ]
        });
    });
    //console.log( b1phase );
}

calcCollisionTime = function( b1phase, b2phase ) {
    var colltime = { 'phaseshift' : [] };
    min = 9.0e99;
    max = -9.0e99;
    for ( var ix=0; ix<3564; ix++ ) {
        var ctime = (b1phase.phaseshift[ix]+b2phase.phaseshift[ix])/2.0
        colltime.phaseshift.push( ctime );
        if (ctime < min)
            min = ctime;
        if (ctime > max)
            max = ctime;
    }
    colltime['min'] = min;
    colltime['max'] = max;
    return colltime;
}

downloadDetuningJson = function( event ) {
    var datastr =  "data:text/json;charset-utf-8," + encodeURIComponent( JSON.stringify( dtdata ) );
    event.currentTarget.href = datastr;
}

makePhasePlots = function( params, beam1, beam2, collisions ) {
//    params contains : 
//    V0     : voltage of one cavity             [V]
//    ppb    : protons per bunch                 [number]
//    f_RF   : RF radio frequency (400800000)    [Hz]
//    b_len  : bunch length in seconds (4 sigma) [sec]
//    RoverQ : R/Q of the cavity: 45             [Ohm]
//    
    dtdata = { "IP1_5" : {},
               "IP2" : {},
               "IP8" : {},
               "Parameters" : params }
              
    //  parameters
    var params = { 'V0' : $('input#cavity_HV').val() * 1000000,
                   'ppb' : $('input#ppb').val(),
                   'f_RF' : 400800000.0,
                   'f_rev' : 11245.5,
                   'RoverQ' : 45,
                   'b_len' : $('input#blen').val() * 1e-9
                 }
    
    var b1phase = calcPhase( beam1, params );
    var b2phase = calcPhase( beam2, params );
    
    
    //console.log( 'collisions', collisions.ATLASCMS[0] );
    // plots for IP1/5
    var colltime = calcCollisionTime( b1phase, b2phase );
    var displacement = calcZoffset( b1phase.phaseshift, b2phase.phaseshift);
    makeChart("detuning1" , "IP1/5 phase related data", b1phase, b2phase, colltime, displacement, collisions.ATLASCMS[0] );

    dtdata["IP1_5"] = { "phase_b1" : b1phase,
                        "phase_b2" : b2phase,
                        "collisionTimePhase" : colltime,
                        "z-displacement" : displacement,
                        "collision_bucket_ids" : collisions.ATLASCMS[0] }
        
    // plots for IP8

    //var ixlhcb = 894;
    var ixlhcb = 2670; // 3564-894
    var phaselhcb = []
    for ( var i=0; i<3564; i++ )
        phaselhcb.push(0);

    // for ( var ix in b1phase.phaseshift ) {
    //     phaselhcb[ixlhcb] = b1phase.phaseshift[ix];
    //     ixlhcb++;
    //     if (ixlhcb == 3564)
    //         ixlhcb = 0;        
    // }
    for ( var ix in b1phase.phaseshift ) {
        phaselhcb[ix] = b2phase.phaseshift[ixlhcb];
        ixlhcb++;
        if (ixlhcb == 3564)
            ixlhcb = 0;        
    }

    //lhcbphase = JSON.parse( JSON.stringify( b1phase ) ); // deep copy
    //lhcbphase.phaseshift = phaselhcb;
    //cl = collisions['LHCb'][1].sort( function(a,b){ return a - b; } );
    //var colltime = calcCollisionTime( lhcbphase, b2phase );
    //var displacement = calcZoffset( lhcbphase.phaseshift, b2phase.phaseshift);

    lhcbphase = JSON.parse( JSON.stringify( b2phase ) ); // deep copy
    lhcbphase.phaseshift = phaselhcb;
    cl = collisions['LHCb'][1].sort( function(a,b){ return a - b; } );
    var colltime = calcCollisionTime( b1phase, lhcbphase );
    var displacement = calcZoffset( b1phase.phaseshift, lhcbphase.phaseshift );

    dtdata["IP8"] = { "phase_b1" : b1phase,
                      "phase_b2" : lhcbphase,
                      "collisionTimePhase" : colltime,
                      "z-displacement" : displacement,
                      "collision_bucket_ids" : cl }

    makeChart("detuning2" , "IP8 phase related data", b1phase, lhcbphase, colltime, displacement, cl, "Bunch crossing id Beam 1 (0 ... 3564)" );

    // plots for IP2
    var ixip2 = 891;
    var phaseip2 = []
    for ( var i=0; i<3564; i++ )
        phaseip2.push(0);
    for ( var ix in b1phase.phaseshift ) {
        phaseip2[ix] = b2phase.phaseshift[ixip2];
        ixip2++;
        if (ixip2 == 3564)
            ixip2 = 0;        
    }

    ip2phase = JSON.parse( JSON.stringify( b2phase ) );
    ip2phase.phaseshift = phaseip2;

    var colltime = calcCollisionTime( ip2phase, b1phase );
    var displacement = calcZoffset( b1phase.phaseshift, ip2phase.phaseshift);

    dtdata["IP2"] = { "phase_b1" : b1phase,
                        "phase_b2" : ip2phase,
                        "collisionTimePhase" : colltime,
                        "z-displacement" : displacement,
                        "collision_bucket_ids" : collisions['ALICE'] }

    makeChart("detuning3" , "IP2 phase related data", b1phase, ip2phase, colltime, displacement, collisions['ALICE'][0], "Bunch crossing id Beam 1 (0 ... 3564)" );

}

calcBands = function( collisions, color ) {
    bands = [];
    var cb = { 'color' : color ,
               'from' : 0 };
    var bx = 0;
    var cbx = 0;
    for ( var ix = 0; ix< collisions.length; ix++ ) {
        bx = (collisions[ix]-1)/10;
        if ( bx == cbx + 1 ) {
            cbx = bx;
            continue;
        } else {
            cb = { 'color' : color,
                   'from' : cbx+0.5,
                   'to' : bx - 0.5
                 }
            cbx = bx;
            bands.push(cb);            
        }
    }
    bands.push( { 'color' : color,
                  'from' : bx + 0.5,
                  'to' : 3564 }
              );
    return bands;
}
