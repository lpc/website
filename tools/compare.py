#!/usr/bin/python
import os
import re
from filecmp import dircmp

EXCEPTIONS = { 'production' : ['git-source', ".DS_Store", '._.DS_Store', "web.config", "before_2016"],
               'git'        : ['.git', '.DS_Store','._.DS_Store'],
               'excl_pat'   : ['.#','jQRangeSlider', 'js/ckeditor', 'editorSchemes', '.*\.pyc'],
               # directories to exclude
               'dir_pat'    : ['.*\.git.*', '.*/internal/?','.*/lpc-minutes-workdir','.*/lpc-page-workdir','.*/tools','.*old/*'],
               # files to exclude
               'file_pat'   : ['.+\.pyc$', '.*DS_Store', '.*\._\.DS_Store','\./js/logon.js','\./js/htmledit.js','\./js/editMinutes.js','\./js/fillNotes.js','\./css/htmledit.css','\./css/editFillTable.css','\./js/fillNotes.js','.*\.gitignore','\./web.configg'] }

pats = []
for pat in EXCEPTIONS['excl_pat']:
    pats.append( re.compile( pat ) );
 
for root, dirs, files in os.walk("."):
    skip = False
    for rex in EXCEPTIONS['dir_pat']:
        if re.match( rex, root ):
            skip = True
            break
    if skip:
        continue

    for pat in EXCEPTIONS['excl_pat']:
        #print " and " + root
        stop = 0
        if pat in root:
            stop = 1
            break
    if stop:
        continue

    
    proddir = "../" + root
    if not os.path.isdir(proddir):
        print "directory ", proddir, "does not exist in production area"
        continue


    dcmp = dircmp( root, "../"+root)

    # check for modified files
    for file in dcmp.diff_files:
        skip = False
        for rex in EXCEPTIONS['file_pat']:
            #print "match " + rex + " agains " + file
            if re.match( rex, file ):
                skip = True                
                break
        if skip:
            continue
        print "modified : ",  root + "/" + file


    for nx in dcmp.left_only:
        if '~' in nx:
            print "backup file ", root + "/" + nx
        else:
            is_exception = False
            for rex in EXCEPTIONS['file_pat']:
                #print "compare ", rex, root + '/' + nx
                if re.match( rex, root + '/' + nx ):
                    is_exception = True
                    break
            for rex in EXCEPTIONS['dir_pat']:
                #print "compare ", rex, root + '/' + nx
                if re.match( rex, root + '/' + nx ):
                    is_exception = True
                    break
            if not is_exception:
                print "non existent in production area: ", root + "/" + nx

    for nx in dcmp.right_only:
        is_exception = False
        for ex in EXCEPTIONS['production'] :
            if nx == ex:
                is_exception = True
                break
        if not is_exception:
            print "non existent in git area: ", "../" + root + "/" + nx

#print "Files which are not equal:"
#for root, dirs, files in os.walk("."):
#    if ".git" in root:
#        continue
##    print os.path.dirname(root)
#    for name in files:
#        if name[-1] == "~":
#            continue
#        if name[0] == ".":
#            continue
#        orig = os.path.join(root,name)
#        web = os.path.join( "../", root, name  )
#
#        if (not filecmp.cmp( orig, web ) ):
#             print orig + " <==> " + web
