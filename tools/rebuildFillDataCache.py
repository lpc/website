#!/usr/bin/python
import json
import os
import urlparse
import sys
sys.path.append( "../../cgi-bin" )
sys.path.append( "../../../cgi-bin" )
from scanScheme import scanFile, analyseScheme


schemeDir = os.getcwd()
print "scanning " + schemeDir

#relPath = "../fillingSchemes"
#schemeName = "2017"
#result = { 'schemes' : {},
#           'relpath' : relPath + "/" + schemeName }

cachefile = os.path.join( schemeDir, "schemeCache.json" )
fd = open( cachefile, "r" )
result = json.load( fd )
fd.close()
result['schemes'] = {}

for scheme in os.listdir( schemeDir ):
    scheme = scheme.strip()
    if scheme.endswith( "csv" ):
        schemepath = os.path.join( schemeDir, scheme )
        txtpath = os.path.splitext(schemepath)[0] + ".txt"            

        nofile = ""
        if not os.path.isfile( txtpath ):
            nofile = " --> NO TXT FILE"
        print "analysing " + schemepath + nofile
 
        injections = scanFile( schemepath )
        analyseScheme( injections )
        
        result['schemes'][scheme] = { 'description'  : injections['description'],
                                      'injections_1' : injections['injections_1'],
                                      'injections_2' : injections['injections_2'],
                                      'collisions'   : injections['collisions'],
                                      'bunches_1'    : injections['bunches']['bunches_1'],
                                      'bunches_2'    : injections['bunches']['bunches_2'],
                                  }

jresult = json.dumps( result )

print jresult

cachefile = os.path.join( schemeDir, "schemeCache.json" )
fd = open( cachefile, "w" )
fd.write( jresult )
fd.close()



