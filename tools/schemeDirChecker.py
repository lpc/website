#!/usr/bin/python
import csv
import re
import urllib2 
import os

YEAR = str(2016)

prodSchemeDir = "../fillingSchemes/" + str(YEAR)
candSchemeDir = prodSchemeDir + "/candidates"

dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + YEAR + "/FillingSchemes.ksv"

infd = urllib2.urlopen( dataurl )
csvreader = csv.reader( infd )
csvreader.next() # get rid of header
for row in csvreader :
    fillno = row[0]
    schemename = row[1] + ".csv"
    path = prodSchemeDir + "/" + schemename
    cpath = candSchemeDir + "/" + schemename
    if not os.path.isfile( path ):
        print schemename + " not in production dir"
        if not os.path.isfile( cpath ):
            print "  !!!! and not in candidate foler !!!!"
        else:
            print "cp " + cpath + " " + path
    
    
