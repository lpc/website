#!/usr/bin/python
from datetime import datetime
import csv
import sys
import os

OUTFILE="FillingSchemes_2016.ksv"

gitpath = "../fillingSchemes/2016"
prodpath = "../../fillingSchemes/2016"


def usage() :
    print "Usage :   analyse.py {filename}"

if len(sys.argv) != 2:
    usage()
    sys.exit(-1)
    
infile = sys.argv[1]

gitfd = open( os.path.join(gitpath,OUTFILE), "w" )
prodfd = open( os.path.join(prodpath, OUTFILE),  "w" )

print gitfd
print prodfd

with open( infile, 'rb' ) as input :
    csvreader = csv.reader( input )
    print "fillnumber,scheme,datetime,duration"
    gitfd.write("fillnumber,scheme,datetime,duration\n")
    prodfd.write("fillnumber,scheme,datetime,duration\n")
    waitstate = '1' # wait for Stable Beams
    fillcount = 0
    tmp = 0
    for row in csvreader :
        row[0] = row[0][:-4]
        if waitstate=='1' and row[3] == '1':
            waitstate = '0'
            starttime = datetime.strptime( row[0],"%Y-%m-%d %H:%M:%S" )
            starttimestr = row[0]
            #            if tmp == row[4]:
            #                print "EEEEEEEEEEEEEE", row[4]
            fillingScheme = row[5]
            tmp = row[4]
            continue
        if waitstate == '0' and row[3] == '0':
            endtime = datetime.strptime( row[0],"%Y-%m-%d %H:%M:%S" )
            waitstate = '1'
            duration = endtime - starttime

#            print row[4]+","+fillingScheme+","+row[0]+","+str(duration)
#            gitfd.write( row[4]+","+fillingScheme+","+row[0]+","+str(duration) + "\n" )
#            prodfd.write( row[4]+","+fillingScheme+","+row[0]+","+str(duration) + "\n" )

            print row[4]+","+fillingScheme+","+starttimestr+","+str(duration)
            gitfd.write( row[4]+","+fillingScheme+","+starttimestr+","+str(duration) + "\n" )
            prodfd.write( row[4]+","+fillingScheme+","+starttimestr+","+str(duration) + "\n" )

            fillcount += 1
            continue
#print fillcount, "fills"
