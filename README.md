# Project lpc/website

This project contains the sources for the lpc website at CERN.
LPC stands for LHC Programme Coordinator.

The site is written "by hand" in html-5. It contains style-sheets
and javascript code.

Initiators of this repository are Christoph Schwick and Jamie Boyd.
(February 2016)


