#!/usr/bin/python
import json
import cgi
import os
import glob
import cgitb
import re
import datetime
import shutil
from checkLogin import checkCookies
import urllib2 

cgitb.enable()

checkCookies("/git-source/internal/cgi-bin/editFillTable.py")


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, ".." )

form = cgi.FieldStorage()

# globals

pdir = os.path.join( pp, "documents", "FillTables")
proddir = os.path.join( pp, "..", "documents", "FillTables")
action = form['action'].value
error = ""



def loadTable():
    # get the filltable generated from timber            
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/FillTable.json"
    infd = urllib2.urlopen( dataurl )
    jsondoc = infd.read()

    timbertab = json.loads( jsondoc )


    # Now get the annotated fill table which we want to edit here
    tablename = "FillTable_" + year + ".json"
    tablepath = os.path.join( pdir, tablename )
    anntab = {}
    if os.path.isfile( tablepath ):
        fd = open( tablepath, "r" )
        ats = fd.read()
        anntab = json.loads( ats )
    
    # Now add all non existing entries of the timber table into the Fill Table
    for fill, entry in timbertab.iteritems() :
        if fill not in anntab:
            anntab[ fill ] = entry
        else:
            # for already existing fills correct some data which might have changed
            anntab[ fill ]['length_sb'] = entry['length_sb'] # this could change if the editor is called
                           # in the middle of a fill which toggles from STABLE BEAMS to ADJUST and back
            anntab[ fill ]['start_sb'] = entry['start_sb']  # in principle this should never change



    # Now make all fills completely editable, which are not in the Timber
    for fill, entry in anntab.iteritems():
        if fill not in timbertab:
            entry['fulledit'] = True
            
    return (anntab, year)



if action == 'load' or action == 'loadro' :
    (data, year) = loadTable();
    res = { "status" : "loaded", 
            "action" : action,
            "data"  : data,
            "year"  : year,
            "error" : error }

elif action == 'save' :
    year = form['year'].value
    datastr =  form['table'].value
    data = json.loads( datastr )
    tablename = "FillTable_" + year + ".json"
    tablepath = os.path.join( pdir, tablename )

    fd = open( tablepath, "w" )
    json.dump( data, fd )
    fd.close()

    res = { "status" : "saved",
            "action" : action,
            "error" : error }

elif action == 'release' :
    year = form['year'].value
    datastr =  form['table'].value
    data = json.loads( datastr )
    tablename = "FillTable_" + year + ".json"
    tablepath = os.path.join( proddir, tablename )

    fd = open( tablepath, "w" )
    json.dump( data, fd )
    fd.close()

    res = { "status" : "released",
            "action" : action,
            "error" : error }
else:
    error =  "action " + action + " not known"
    res = { "status" : "problem" }
    

print "Content-type: application/json"
print 
print json.dumps( res )

