#!/usr/bin/python
import json
import cgi
import os
import sys
import glob
import cgitb
import re
import shutil
from checkLogin import checkCookies
import datetime
import urllib2 

cgitb.enable()

checkCookies("/git-source/internal/cgi-bin/editEventAnnotations.py")


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, ".." )

form = cgi.FieldStorage()

# globals

pdir = os.path.join( pp, "documents", "EventAnnotations")
proddir = os.path.join( pp, "..", "documents", "EventAnnotations")
action = form['action'].value
error = ""


def loadAndFormatFillTable( year ):
    '''Loads and formats a fill table: an dict of "fill-dicts" with the keys 

    'fill' (int fillno)
    'starttime' (datetime in python)
    'endtime' (datetime in python)

    The keys are the fillnumbers as string'''

    ftname = os.path.join( pp, "documents", "FillTables", "FillTable_" + year + ".json")
    ftd = open( ftname, 'r' )
    ftraw = json.load( ftd )
    ftd.close()
    ft = {}
    for fillno,fh in ftraw.iteritems():
        fill = { 'fill' : int(fillno) }
        startTime = datetime.datetime.strptime( fh['start_sb'], "%Y-%m-%d %H:%M:%S" )
        mo = re.match( '((\d+) days?, )?(\d+):(\d\d):(\d\d)', fh['length_sb'])
        if not mo:
            sys.exit()
        if mo.group(2) == None:
            days = 0
        else:
            days = int(mo.group(2))
        hours = int(mo.group(3))
        minutes = int(mo.group(4))
        seconds = int(mo.group(5))

        if days:
            hours += 24 * days

        delta = datetime.timedelta( hours=hours, minutes = minutes, seconds = seconds )
        endTime = startTime + delta
        fill['starttime'] = startTime
        fill['endtime'] = endTime
        ft[ fillno ] = fill
    
    fillarr = sorted(ft.keys(),key=int)
    return (ft, fillarr)

def insertMissingInfo( year, data ):
    (ft,fillarr) = loadAndFormatFillTable( year )
            
    for rec in data:
        # if the times are missing but the dates are there then assume
        # midnight as a time. If times are inserted with no dates, treat
        # this as if there was not date/time pair entered (this is a mistake
        # of the user.)
        if rec['start_date'] and not rec['start_time']:
            rec['start_time'] = "00:00"
        if rec['end_date'] and not rec['end_time']:
            rec['end_time'] = "23:59"

        # if the fill number is given but not the corresponding time:
        # enter start time or end time resp. for the fill
        if rec['start_fillno'] and not rec['start_date'] :
            if rec['start_fillno'] in ft:
                rec['start_date'] = ft[rec['start_fillno']]['starttime'].strftime( "%Y-%m-%d" )
                rec['start_time'] = ft[rec['start_fillno']]['starttime'].strftime( "%H:%M" )
            else:
                rec['end_date'] = "invalid start fillno"

        if rec['end_fillno'] and not rec['end_date'] :
            if rec['end_fillno'] in ft:
                rec['end_date'] = ft[rec['end_fillno']]['endtime'].strftime( "%Y-%m-%d" )
                rec['end_time'] = ft[rec['end_fillno']]['endtime'].strftime( "%H:%M" )
            else:
                rec['end_date'] = "invalid end fillno"
        
        # if start_date/time is given but no fillno 
        laststart_fno = 0
        lastend_fno = 0
        if rec['start_date'] and not rec['start_fillno']:
            startdt = datetime.datetime.strptime( rec['start_date'] + " " + rec['start_time'], "%Y-%m-%d %H:%M" )
            for fillno in fillarr:
                record = ft[fillno]
                if record['endtime'] < startdt:
                    lastend_fno = fillno
                    continue

                if record['starttime'] > startdt:
                    # the next fill is starting after the time: take last fillno + 1
                    rec['start_fillno'] = str(int(lastend_fno) + 1)
                else:
                    # we have a time in the middle of the fill -> take this fill
                    rec['start_fillno'] = fillno
                break

        # if end_date/time is given but no fillno
        laststart_fno = 0
        lastend_fno = 0
        if rec['end_date'] and not rec['end_fillno']:
            enddt = datetime.datetime.strptime( rec['end_date'] + " " + rec['end_time'], "%Y-%m-%d %H:%M" )
            for fillno in fillarr:
                record = ft[fillno]
                if record['endtime'] < enddt:
                    lastend_fno = fillno
                    continue

                if record['starttime'] > enddt:
                    # the next fill is starting after the time: take fillno - 1
                    rec['end_fillno'] = str(int(fillno) - 1)
                else:
                    # we have a time in the middle of the fill -> take this fill
                    rec['end_fillno'] = fillno
                break

###################################################################



if action == 'load' or action == 'loadro' :
    year = form['year'].value

    tablename = "EventAnnotations_" + year + ".json"
    tablepath = os.path.join( pdir, tablename )
    data = {}
    status = "new table created"

    if os.path.isfile( tablepath ):
        fd = open( tablepath, 'r' )
        data = json.load( fd )
        fd.close()
        status = "loaded"
    res = { "status" : status, 
            "action" : action,
            "data"  : data,
            "year"  : year,
            "error" : error }

elif action == 'save' :
    year = form['year'].value
    datastr =  form['table'].value
    data = json.loads( datastr )
    tablename = "EventAnnotations_" + year + ".json"
    tablepath = os.path.join( pdir, tablename )

    insertMissingInfo( year, data )

    fd = open( tablepath, "w+" )
    json.dump( data, fd )
    fd.close()

    res = { "status" : "saved",
            "action" : action,
            "data"   : data,
            "error"  : error }

elif action == 'release' :
    year = form['year'].value
    datastr =  form['table'].value
    data = json.loads( datastr )
    tablename = "EventAnnotations_" + year + ".json"
    tablepath = os.path.join( proddir, tablename )

    fd = open( tablepath, "w" )
    json.dump( data, fd )
    fd.close()

    res = { "status" : "released",
            "action" : action,
            "data"   : data,
            "error" : error }
else:
    error =  "action " + action + " not known"
    res = { "status" : "problem" }
    

print "Content-type: application/json"
print 
print json.dumps( res )

