#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import json
from checkLogin import checkCookies
from datetime import datetime

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

checkCookies("/git-source/internal/cgi-bin/editCommissioningPage.py")

form       = cgi.FieldStorage()
if 'year' in form:
    year       = form['year'].value
else:
    year = str(datetime.now().year)


commissioningfile = os.path.join( pp, "..", "documents", "commissioning",  "commissioning_" + year + ".json" )


commData =[]
if os.path.isfile( commissioningfile ) :
    fd = open( commissioningfile, 'r' )
    commData = json.load( fd )
    fd.close()


htmlout = '''Content-Type: text/html

<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>Edit the LPC commissioning page</title>
    <link rel="stylesheet" type="text/css" href="../../css/lpc.css">
    <script src="../../js/jquery-2.2.0.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/utils.js"></script>
    <script src="../../js/commissioningPage.js"></script>
    <script>
       var year = "'''
htmlout += year
htmlout += '''";
    </script>
  </head>

  <body>
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Edit Commissioning Page ''' + year + '''
	  </p>
	  <p class="center">
	    <a href="../../Default.htm">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../static/Default.htm">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="warning"></div>
    <div id="debug"></div>
    <div id="contents">

<div id="status"></div>


    <table id="commissioningtab" class="commissioning">
      <thead>
        <tr>
         <th style="width:155px">Date (for sorting)</th>
         <th style="width:155px">Date string</th>
         <th style="width:200px">Task</th>
         <th>Details</th>
        </tr>
      </thead>
      <tbody>
'''
for item in commData:
    htmlout += '<tr><td data-name="date"><input type="date" value="'+item['date']+'" onchange="dateChange( event )" /></td> <td data-name="datestr" contenteditable="true">'+item['datestr']+'</td><td data-name="title" contenteditable="true">'+item['title']+'</td><td data-name="description" contenteditable="true">'+item['description']+'</td></tr>'

htmlout += '''
      </tbody>
    </table>
<p>
<button onclick="addempty()">Add empty row</button>
<button onclick="savetab('save')">save</button>
<button onclick="savetab('release')">release</button>
</p>
<p>
<em>To delete an item simply remove the date in the left-most input field before saving.<br>
To sort the items according to the date, just save the table and reload the page.</em>
</p>
</div>
  </body>
</html>
'''

print htmlout
