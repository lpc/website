import cgi
import cgitb
import os
from checkLogin import checkCookies

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))
    
params = cgi.FieldStorage()
url = params['url'].value

thisurl = os.environ["SCRIPT_NAME"] + "?url=" + url

checkCookies( thisurl )

sfile = os.path.join( pp, url )

fd = open(sfile,'r')
response = fd.read()
fd.close()


print 'Content-type: text/html'
print
print response
