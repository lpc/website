#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import re
from checkLogin import checkCookies
from datetime import datetime

RELPATTERN = re.compile( ".+(fillingSchemes.+)")

def procDir( cdir ):
    out = ""
    for dir in os.listdir( cdir ):
        if os.path.isdir( os.path.join( cdir,dir) ):
            if dir == 'csvlock_taken' or dir == 'csvlock_free':
                continue
            out +=  "<li>" + dir + '<ul class="noborder">'
            out += procDir( os.path.join( cdir, dir ) )
            out +=  "</ul></li>"
    for scheme in os.listdir( cdir ):
        if scheme.endswith( "csv" ):
            mo = RELPATTERN.match( cdir );
            relpattern = "."
            if mo:
                relpattern = mo.group(1)
            out += '<li class="scheme" data-relpath="' + relpattern + '" data-path="' + cdir + '">' + scheme + "</li>"
    return out

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

checkCookies("/git-source/internal/cgi-bin/editCommissioningPage.py")

schemeDir = os.path.join( pp, "..", "fillingSchemes" )
#
#form       = cgi.FieldStorage()
#if 'schemeFile' in form:
#    schemeFile = form['schemeFile'].value
#    schemepath = os.path.join( schemeDir,  schemeFile )
#    txtfile = os.path.splitext( schemepath )[0] + ".txt"
#    if os.path.isfile( txtfile ) :
#        fd = open( txtfile, 'r' )
#        txt = fd.read() 
#        fd.close()
#    else:
#        txt = ""
#else:
#    schemeFile = None
#    schemepath = None
#    txtfile = None
#    txt = ""


htmlout = '''Content-Type: text/html

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>Edit a text annotation for a filling scheme</title>
    <link rel="stylesheet" type="text/css" href="../../css/lpc.css">
    <link rel="stylesheet" type="text/css" href="../../css/lhc.css">
    <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
    <script src="../../js/jquery-2.2.0.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/utils.js"></script>
    <script src="../../js/fillNotes.js"></script>
  </head>

  <body>
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Edit a Filling Scheme Annotation
	  </p>
	  <p class="center">
	    <a href="../../Default.htm">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../static/Default.htm">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="warning"></div>
    <div id="debug"></div>

    <div id="status"></div>
    <div id="contents">

      <div id="schemeMenu">
        <ul id="uschemeMenu">
'''  

htmlout += procDir( schemeDir )

htmlout +=  '''
        </ul>
      <script>
        $('div#schemeMenu li').on( "click", function(event) {
          event.stopPropagation();
          postRequest( "load", { schemeName : event.target.textContent, schemePath : event.target.getAttribute( 'data-path' ), relPath : event.target.getAttribute( 'data-relpath' ) });
          });
        $(function() { $('ul#uschemeMenu').menu({position: {my: "left top", at: "right top" } }); } ); 
      </script>
    </div>

    <p id="schemeName"></p>
    <p id="schemeAnnotationText" contenteditable="true">
    <script>
       $('p#schemeAnnotationText').on('keydown', function() {
           $('div#status').html("changed");
           changed = true;
         });
    </script>
   </div>
<p style="text-align: center">
<button onclick="releaseAnnotation()" title="Copy annotation to production area">release</button>
</p>
<div style="height:50px;"></div>

<hr style="width:80%">
<div style="width: 80%; margin:auto;">
<p>
<h2>Upload new csv file</h2>
<p>
This allows to upload csv files with filling schemes. Files will be copied to the 
development AND productions area. The server checks that the file does not yet exist on the 
server side. It refuses to overwrite an existing file if the overwrite option is not checked.
Files larger than 10MB 
will NOT be uploaded (normal csv files are around 6 MB at the time of writing of this page). 
</p>
<form id="uploadcsv" enctype="multipart/form-data" action="fillNotesHandler.py" method="post">
<table>
<tr><td>
<label>Year</label></td><td> : </td><td><select name="year" id="yearselect">
   <option value="2015">2015</option>
   <option value="2016">2016</option>
   <option value="2017">2017</option>
   <option value="2018">2018</option>
   <option value="2021" selected>2021</option>
</select></td></tr>
<tr>
<td><label>Candidate folder</label></td><td> : </td><td><input name="candidate" type="checkbox" value='1' checked /></tr>
<tr>
<td><label>Force overwrite</label></td><td> : </td><td><input name="overwrite" type="checkbox" value='1'  /></tr>
<tr>
<td><label>File to upload</label></td><td> : </td><td><input type="file" name="csvfile"/></td></tr>
<tr>
<td></td><td></td><td><input type="submit"></td></tr>
</table>
</form>
</div>
<script>
$('form#uploadcsv').submit( uploadCSV );
</script>
</body>
</html>
'''

print htmlout
