#!/usr/bin/python
import json
import cgi
import os
import sys
import cgitb
from fsutils import getJsonFile, saveJsonFile
from checkLogin import checkCookies

cgitb.enable()
    
if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form       = cgi.FieldStorage()

# NOTE: the calendar.json file is always saved into the document tree of the production 
#       version and into the correspoding directory in the git-source. Like thsi it is 
#       saved into the git repository.

calpath = os.path.join( pp, "..", "..", "documents", "ToyCalendars", "calendars.json" )
calpath_dev = os.path.join( pp, "..", "documents", "ToyCalendars", "calendars.json" )

action = form['action'].value


schemeName = ""
retdat = []
dbx = ""
error = "no calendars"
calendars = ()

if action == "saveCalendar":
    checkCookies()
    key = form['key'].value
    calendar = json.loads(form['calendar'].value)
    (calendars,error) = getJsonFile( calpath )
    calendars[key] = calendar
    saveJsonFile( calendars, calpath )
    saveJsonFile( calendars, calpath_dev )
    dbx = calpath
elif action == "deleteCalendar":
    checkCookies()
    key = form['key'].value
    (calendars,error) = getJsonFile( calpath )
    if not key in calendars:
        error = "The key " + str(key) + " does not exist in the calendar database."
    else:
        del calendars[key]
        saveJsonFile( calendars, calpath )
        saveJsonFile( calendars, calpath_dev )

response = { "debug"  : dbx,
             "error"  : error,
             "data"   : calendars,
             "action" : action }

print "Content-type: application/json"
print 
print json.dumps( response )

