#!/usr/bin/python
import cgitb
import cgi
import os
import re
import json
import glob
from checkLogin import checkCookies

cgitb.enable()

checkCookies()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))


# get the list of previously edited minutes
#pdir = os.path.join( pp, "lpc-minutes-workdir")
pdir = os.path.join( pp, "..", "lpc-minutes-workdir")
jsnames = os.path.join( pdir, "*.json")
jsfiles = glob.glob( jsnames )
nfiles = []
for fn in jsfiles:
    nfiles.append(os.path.basename(fn))
htmlout = '''Content-Type: text/html

'''

fd = open( os.path.join( pp, "..", "calendar.html" ),'r')
calhtml = fd.read()
fd.close()

calhtml = calhtml.replace( '</title>\n','</title>\n<base href="https://lpc.web.cern.ch/git-source/">\n')
calhtml = calhtml.replace( 'LPC home</a>', 'LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="internal/static/Default.htm">Internal pages</a>' )

admintools = '''
<table>
<tr><td><label>Calendar name</label></td><td><input id="calname" type="text"/></td></tr>
<tr><td><label>Year</label></td><td><input id="calyear" type="number"/></td></tr>
<tr><td><label>Label</label></td><td><input id="callabel" type="text"> (for key in json file; format: yyyy-v1-v2) </td></tr>

<tr><td></td><td> <button onclick="saveCalendar()">Save/update</button> <button onclick="deleteCalendar()">Delete</button> </td></tr>
</table>
<hr>
'''

calhtml = calhtml.replace( "<!-- page header end -->", "<!-- page header end -->\n" + admintools )

htmlout += calhtml


print htmlout

