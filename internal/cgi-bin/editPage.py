#!/usr/bin/python
import cgitb
import cgi
import os
import re
import json
import glob
from shutil import copyfile
from checkLogin import checkCookies

cgitb.enable()

# first check if we have a logon cookie
checkCookies()

# the base for this page will be set to the directory of the page to edit. 
baseurl = "https://lpc.web.cern.ch/git-source/"
url = ""
toBase = ""
head = ""
body = "nothing"
error = ""

def makeBackup( filepath, txt ):
    filename = os.path.basename( filepath )
    bckpath = os.path.join( pw, filename ) + ".bck_*"
    bcks = glob.glob( bckpath )
    if bcks:
        bcks.sort( key=os.path.getmtime)
        bck = bcks[-1]
        mo = re.match( r'.+\.bck_(\d)$', bck )
        if mo:
            no = (int(mo.group(1))+1) % 10
        else:
            # This should not happen: would be better to handle this correctly: it is a bug
            # or somebody put "dirt" in the directory...
            no = 0
        bck = os.path.join( pw, filename ) + ".bck_" + str(no)
    else:
        # This is for the first backup, i.e. the first time we edit this page
        bck = os.path.join(pw, filename) + ".bck_0"

    # write the backup file
    fd = open( bck, "w" )
    fd.write( txt )
    fd.close
    return bck


def loadPage( path ):
    # the path under the pp
    basedir = os.path.dirname( path )
    wdir = basedir
    baseurl = "https://lpc.web.cern.ch/git-source/" + basedir + "/"
    ilevel = 0
    while wdir:
        (head,tail) = os.path.split( wdir )
        wdir = head
        ilevel += 1

    toBase = ""
    for il in range( 0,ilevel ):
        toBase += "../"
        
    filepath = os.path.join( pp, path )
    fd = open( filepath, "r" )
    txt = fd.read()
    fd.close()
    url = "https://lpc.web.cern.ch/git-source/" + path 
    makeBackup( filepath, txt )

    # The Default.htm page has attributes in the body tag:
    mo = re.match(r'(.+<body[^>]*>)(.+)</body>', txt, re.DOTALL)
    if mo:
        head = mo.group(1)
        body = mo.group(2)
    else:
        head = ""
        body = "nothing"

    return ( head, body, baseurl, toBase, url );


#################

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, ".." )

pw = os.path.join( pp, "lpc-page-workdir")

form = cgi.FieldStorage()
if 'action' in form:
    action = form['action'].value
else:
    action = "noaction"


if action == "loadpage":
    partialPath = form['page'].value
    ( head, body, baseurl, toBase, url ) = loadPage( partialPath )
    
elif action == "newPage":
    head = ""
    body = "nothing"
    error = ""
    partialPath = form['newPageName'].value
    partialdir = os.path.dirname( partialPath )
    basepath = os.path.join( pp, partialdir )
    path = os.path.join( pp, partialPath )
    # check if dir exists
    if not os.path.isdir( basepath ):
        error = "The directory you specified does not exist!"
    elif os.path.isfile( path ):
        error = "The file already exists: you ave to specifiy a new non-existing filename!"
    else :
        tplt = os.path.join(pw, "template.htm")
        tfd = open( tplt, "r")
        template = tfd.read()
        tfd.close()
        
        nfd = open( path, 'w')
        nfd.write(template)
        nfd.close()

        ( head, body, baseurl, toBase, url ) = loadPage( partialPath )
        template = re.sub( r'__to_base__', toBase, template )
        nfd = open( path, 'w')
        nfd.write(template)
        nfd.close()
        ( head, body, baseurl, toBase, url ) = loadPage( partialPath )

        
filepaths = []
filenames = []
# get the list of static pages to edit. Exclude the minutes for which we have an exclusive editir
for (dirpath, dirnames, fns) in os.walk(pp):
    fps = [ os.path.join( dirpath, f )  for f in fns if f.endswith(".htm") if not dirpath.startswith( os.path.join(pp, "lpc-minutes")) ]
    filepaths.extend(fps)

filenames = [ os.path.relpath( f, pp ) for f in filepaths ]

htmlout = ""
htmlout += '''Content-Type: text/html

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit an LPC web page</title>
'''
htmlout += '<base href="'+ baseurl +'">'
htmlout += '''
  <link rel="stylesheet" type="text/css" href="__to_base__css/lpc.css">
  <link rel="stylesheet" type="text/css" href="__to_base__css/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="__to_base__css/htmledit.css">
  <script src="__to_base__js/jquery-2.2.0.min.js"></script>
  <script src="__to_base__js/jquery-ui.min.js"></script>
  <script src="__to_base__js/ckeditor/ckeditor.js"></script>
  <script src="__to_base__js/htmledit.js"></script>
</head>
<body>
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="__to_base__images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Static Page Editor
	  </p>
	  <p class="center">
	    <a href="__to_base__Default.htm">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="__to_base__internal/static/Default.htm">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="__to_base__images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error">
'''
if error:
    htmlout += error
htmlout += '''</div>
    <div id="warning">
    </div>
    <div id="debug"></div>'''
htmlout += '''
    <div id="contents">
'''
if (action == "loadpage") or (action == "newPage"):
    # the json encoding is necessary since otherwise the strings will be interpreted (i.e. '\' dissapears)
    headstr = json.dumps( head );
    # avoid misinterpretation of </script> by the browser
    headstr = re.sub( r"</script>", "<\\/script>", headstr, flags=re.I);
    htmlout += "<script>\nvar head = " + headstr + ";\n var pagefile=" + json.dumps(partialPath) + ";\n</script>"

htmlout = re.sub( r'__to_base__', toBase, htmlout )
formhtml = '''
<form name="editPage" id="editPage" method="post" >
<input name="action" id="action" type="hidden" value=""/>
<div style="display:inline-block">
<h2>Editor actions</h2>
<table>
<tr><td><label>Create new page (path/name): </label></td><td><input name="newPageName" id="newPageName" type="text" value=""/><input type="button" value="Go" onclick="newPage()"></td></tr>
<tr><td><label>Load page: </label></td><td><select name="page" id="page" onchange="loadPage()">'''
formhtml += "<option label=\" \" value='' selected > </option>"
for file in filenames:
    formhtml += "<option value="+ file +">"+ file +"</option>"
formhtml += '''</select>
</td></tr>
<tr><td>Copy git-source version to production site: </td><td><input type="button" value="Release" onclick="release()"></td></tr>
'''
if url <> "" :
    formhtml += '<tr><td></td><td><a href="'+url+'" target="_blank">Link to page being edited</a></td></tr>'

formhtml += '''
</table>
</div>
</form>
<p>&nbsp;</p><div id="status"></div>
'''


#htmlout += str(filenames)
#htmlout += str(filepaths)
htmlout += formhtml
htmlout += '''
<div id="pagetext" contenteditable="true">'''

if (action == 'loadpage') or (action == 'newPage'):
    htmlout += body
    htmlout += '''</div>\n<script>
    editor = CKEDITOR.inline( 'pagetext');
    editor.resetDirty();
    editor.on('key', function() {
    $('div#status').html("changed");
    });        
    runEditor( );
    $('div#status').html("loaded");
    </script>
'''
htmlout += '''</div></div></body>
</html>
'''
print htmlout

