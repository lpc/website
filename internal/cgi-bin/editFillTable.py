#!/usr/bin/python
import cgitb
import cgi
import os
import re
import json
import urllib2
import glob
import time
import shutil
import datetime
from checkLogin import checkCookies
from Lock import Lock

cgitb.enable()
checkCookies()

# When this website is requested the filling schemes of all fills which have
# reached stable beams are copied from the candidates directory fo the main
# directory of the year (in case this has not been done previously). Therefore
# at the end of the year (and provided that this page is requested after the
# end of the run) all schemes which have been used should be in the main
# directory and all schemes which are in the candidates folder have not been
# used (at least not in fills which reached stable beams)

def reordercsvfiles( path ):
    candpath = os.path.join( path, "candidates" )

    
    logfile = os.path.join( candpath, "lock_activity.log" )
    locker = Lock( "csvlock", candpath, logfile )

    candp = glob.glob( os.path.join(candpath, "*.csv" ))
    cands = []
    for c in candp:
        cands.append(os.path.splitext(os.path.basename( c ))[0])
    tomove = []
    for fill,entry in filltab.iteritems():
        if entry['scheme'] in cands and not entry['scheme'] in tomove:
            tomove.append( entry['scheme'] )
    for scheme in tomove:
        # Now we have to remove the entry in the cache from the src
        # This is not threadsafe!!! Potential problem !!!
        # get the lock 

        if not locker.takelock():
            return False

        # move the files and change the timestamps in order to get them
        # re-cached at the destination.
        src = os.path.join( candpath, scheme + ".csv" )
        dst = os.path.join( path, scheme + ".csv" )
        shutil.move( src, path )
        with open( dst, 'a' ):
            os.utime( dst, None )
        src = os.path.join( candpath, scheme + ".txt" )
        dst = os.path.join( path, scheme + ".txt" )
        if os.path.isfile( src ):
            shutil.move( src, path )
            with open( dst, 'a' ):
                os.utime( dst, None )

        locker.log( "moved the files" )
        fd = open( os.path.join( candpath, "schemeCache.json" ), 'r' )
        ccache = json.load(fd)
        fd.close()
        locker.log( "loaded the schemeCache" )
        if ( (scheme + '.csv') in ccache['schemes'] ):
            del ccache['schemes'][scheme + '.csv']
        locker.log( "deleted entries" )
        fd = open( os.path.join( candpath, "schemeCache.json" ), 'w' )
        json.dump( ccache, fd )
        fd.close()
        locker.log( "dumped the cache" )

        # give the lock back
        locker.givelock()        

    return True
        
if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

year = str(datetime.datetime.now().year)
devpath  = os.path.join( pp, "..", "fillingSchemes", year )
prodpath = os.path.join( pp, "..", "..", "fillingSchemes", year )

# get the filltable from this year
dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/FillTable.json"
try:
    infd = urllib2.urlopen( dataurl )
    filltab = json.load(infd)
except urllib2.HTTPError, e:
    error = repr(e)
    filltab = "{}"
    
errtxt = ""

fl1 = reordercsvfiles( devpath )
fl2 = reordercsvfiles( prodpath )

if (not fl1) or (not fl2):
    errtxt = "Something went wrong when locking files. Check the locker log file " + str(fl1) + "  " + str(fl2)

htmlout = '''Content-Type: text/html

<!DOCTYPE HTML>
<html lang="en">
<head>
  <title>Fill Table Editor</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../../css/lpc.css">
  <link rel="stylesheet" type="text/css" href="../../css/fillTable.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
  <script src="../../js/jquery-2.2.0.min.js"></script>
  <script src="../../js/jquery-ui.min.js"></script>
  <script src="../../js/ckeditor/ckeditor.js"></script>
  <script src="../../js/editFillTable.js"></script>
</head>
<body onload="changeYear()">
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Edit FillTable
	  </p>
	  <p class="center">
	    <a href="../../Default.htm">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../static/Default.htm">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error">'''
htmlout += errtxt
htmlout += '''</div>
    <div id="warning"></div>
    <div id="debug">'''
htmlout += errtxt
htmlout += '''</div>
    <div id="contents">

<div id="status"></div>

<form name="editFillTable" id="editFillTable">
<label>Year : </label>
  <select id="yearselect" onchange="changeYear()">
    <option value="2015">2015</option>
    <option value="2016">2016</option>
    <option value="2017">2017</option>
    <option value="2018" selected >2018</option>
  </select>
</form>

<p>
<button onclick="saveTable()" title="Save to development site">save table</button>
<button onclick="saveTable( 'release' )" title="Save to production site">release table</button>
<button onclick="addRow()">add a row</button>
</p>
<div id="filltable">
<table id="FillTable">
  <thead>
  <tr>
    <th>Fill</th><th style="width: 150px;">Start</th><th>length</th><th>type</th><th>scheme</th><th>end</th><th>ta</th><th>fl</th><th>remarks</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>
<p>
<button onclick="saveTable()" title="Save to development site">save table</button>
<button onclick="saveTable( 'release' )" title="Save to production site">release table</button>
<button onclick="addRow()">add a row</button>
</p>
<p>
<em>
<p>To add a fill by hand (e.g. special runs without Stable Beams) edit all fields after clicking on "Add emtpy row". This fill will stay "fully editable" (opposed to fills found in Timber where some data taken from Timber is not editable). Fills which have been added by hand can be removed by just deleting the fill number and saving the table.
</p>
<p>
<table>
<tr><th>ta</th><th>:</th><td>If set, the turn around time BEFORE this fill should not be taken into account in statistics plots</td></tr>
<tr><th>fl</th><th>:</th><td>If set, this fill should not be taken into account in plots related to fill-length.</td></tr>
</table>
</p>
</em>
</p>
</div>
</body>
</html>
'''
print htmlout

