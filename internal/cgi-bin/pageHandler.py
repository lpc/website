#!/usr/bin/python
import json
import cgi
import os
import glob
import cgitb
import re
import datetime
import shutil
from checkLogin import checkCookies

cgitb.enable()

checkCookies("/git-source/internal/editPage.py")

print "Content-type: application/json"
print 


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join(pp, "..")

form = cgi.FieldStorage()

pw = os.path.join( pp, "lpc-page-workdir")

pf = os.path.join( pw, "currentPage.json" )

action = form['action'].value

def loadcurrentpage():
    fd = open( pf, "r" )
    res = json.load( fd )
    fd.close()
    return res


if action == 'save':
    text = form['text'].value
    pagefile = form['pagefile'].value
    head = form['head'].value
    datarecord = { 'text' : text }
    fd = open( pf, "w" )
    json.dump( datarecord, fd )
    fd.close()
    fn = os.path.join(pp, pagefile )
    fd = open( fn, "w" )
    fd.write( head )
    fd.write( text )
    fd.write( "</body>\n</html>\n" )
    fd.close()
    res = { "status" : "saved" }
#            "debug" : pagefile }


elif action == 'publish':
    # read in template
    tfn = os.path.join( pp, "lpc-minutes")
    tfn = os.path.join( tfn, "template.htm" )
    tfd = open( tfn, "r")
    template = tfd.read()
    tfd.close()
    # read the current minutes dictionary
    cm = loadcurrentminutes()
    # replace date
    datstr = formatDate( cm['date'] )
    template = re.sub( r'__date__', datstr, template, re.MULTILINE ) 
    # replace indico frame
    template = re.sub( r'__indicourl__', cm['indicourl'], template, re.MULTILINE )
    # replace purpose
    template = re.sub( r'__purpose__', cm['purpose'], template, re.MULTILINE )
    # insert text
    template = re.sub( r'__text__', cm['text'], template, re.MULTILINE )

    nfn = os.path.join( pp, "lpc-minutes-workdir")
    nfn = os.path.join( nfn, formatDate( cm['date'], True ) + ".htm" )
    nfd = open( nfn, "w" )
    nfd.write( template )
    nfd.close()
    res = { "status" : "published" }

elif action == "release" :
    pagefile = form['pagefile'].value
    fn = os.path.join(pp, pagefile )

    dest = os.path.join( pp, "..", pagefile )
    if not os.path.isfile( dest ):
        error = "The file " + dest + " does not exist! You cannot release it!"
        res = { 'status' : "problem",
                'error'  : error }
    else:
        shutil.copy( fn, dest)
        res = { 'status' : 'released' }

else:
    res = { "status" : "not saved" }

#print "Content-type: application/json"
#print 
print json.dumps( res )

