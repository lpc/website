import json
import urllib
import os
import Cookie
import datetime
import string
import sys
from random import choice

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

cookieFile = os.path.join( pp, "data/cookies.json" )

def loadValidCookies():
    fd = open(cookieFile, 'r')
    old_valid_cookies = json.load( fd )
    fd.close()
    # remove all cookies which are not valid anymore
    now = datetime.datetime.utcnow()
    valid_cookies = {}
    changed = False
    for cn in old_valid_cookies:
        co = old_valid_cookies[cn]
        expire = datetime.datetime.strptime( co['expires'], '%a, %d %b %Y %H:%M:%S %Z' )
        if now <= expire:
            valid_cookies[cn] = co
        else:
            changed = True
    if changed:
        writeValidCookies( valid_cookies )
            
    return valid_cookies

def writeValidCookies( valid_cookies):
    fd = open(cookieFile, 'w')
    json.dump( valid_cookies,fd )
    fd.close()
    return

def checkCookies( redirecturl = None ):
    cok = Cookie.SimpleCookie( os.environ.get( "HTTP_COOKIE", "" ) ) # second value is default value
    if (cok == "") or (not "lpc_session" in cok):
        # redirect to login with the url
        redirect( redirecturl )
    else:
        session = cok['lpc_session']
        valid_cookies = loadValidCookies()
        if session.value in valid_cookies:
            return True
        else:
            redirect( redirecturl ) # redirect to login with url

def redirect( relurl ):
    #url = os.environ["REQUEST_URI"]
    relurl = relurl or os.environ["SCRIPT_NAME"]
    location = "http://lpc.web.cern.ch/git-source/internal/cgi-bin/authentic.py?"+ urllib.urlencode( { 'url' : relurl } )
    print "HTTP/1.1 302 Found"
    print "Location: " + location
    print "Connection: close"
    print ""
    sys.exit()

def redirectWithNewCookie( url, logonType ):
    valid_cookies = loadValidCookies()
    if "lpc_session" in valid_cookies:
        cookie = valid_cookies['lpc_session']
    else:
        # random string
        value = ""
        for i in range(128):
            value += choice( string.ascii_letters)
        
        now = datetime.datetime.utcnow()
        if logonType == "long":
            delta = 7*24
        else:
            delta = 24
        then = now + datetime.timedelta( hours = delta )
        # set the expiration time to 3h00 to 4h00 either of the same day or the next day
        # (whatever is closer) in order to minimise the chance that an ongoing editing
        # session is interrupted by the invalidation of a cookie.
        hour = then.hour
        diff = hour - 3
        if hour >= 15 :
            delta = 27-hour
        elif hour <= 3:
            delta = 3 - hour
        else:
            delta = 3 - hour

        then = then + datetime.timedelta( hours = delta )

        cookie = { 'value' : value,
                   'path' : "git-source/internal",
                   'expires' : then.strftime('%a, %d %b %Y %H:%M:%S') + " GMT"
        }
        valid_cookies[cookie['value']] = cookie
        writeValidCookies( valid_cookies)

    co = Cookie.SimpleCookie()
    co["lpc_session"] = cookie['value']
    co["lpc_session"]['path'] = cookie['path']
    if logonType == "long":
        co["lpc_session"]['expires'] = cookie['expires']

    return co
