#!/usr/bin/python
import json
import cgi
import os
import glob
import cgitb
import re
import datetime
import shutil
from checkLogin import checkCookies
import urllib2 

cgitb.enable()

checkCookies("/git-source/internal/cgi-bin/editCommissioningPage.py")


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, ".." )

form       = cgi.FieldStorage()

action = form['action'].value
error = ""

if action == 'save' or action == 'release':
    year = form['year'].value
    up = ""
    status = "saved"
    if action == "release":
        status = "released"
        up = ".."
    commissioningfile = os.path.join( pp, up, "documents", "commissioning",  "commissioning_" + year + ".json" )

    datastr =  form['table'].value
    data = json.loads( datastr )

    fd = open( commissioningfile, "w" )
    json.dump( data, fd )
    fd.close()

    res = { "status" : status,
            "action" : action,
            "error" : error }
else:
    error =  "action " + action + " not known"
    res = { "status" : "problem" }
    

print "Content-type: application/json"
print 
print json.dumps( res )

