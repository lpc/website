import cgi
import cgitb
import os
import json
import Cookie
import checkLogin

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

def readSecrets():
    sf = os.path.join( pp, "data", "secrets.json")
    fd = open( sf, "r" )
    secrets = json.load( fd )
    fd.close()
    return secrets
    
params = cgi.FieldStorage();
redirect = "none"

dbtxt = ""
if "url" in params:
    redirect = params['url'].value
    #dbtxt = redirect

cookie = ""
if "user" in params and "password" in params:
    user = params["user"].value
    password = params["password"].value
    logonType = params["logonType"].value
    secrets = readSecrets()
    if user in secrets and secrets[user]['password'] == password:
        cookie = checkLogin.redirectWithNewCookie( redirect, logonType )


print 'Content-Type: text/html'
if cookie:
    print cookie

print '''
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title> LPC login </title>
<script src="../../js/logon.js"></script>
<script src="../../js/jquery-2.2.0.min.js"></script>
</head>
'''
if cookie: 
    #print '<body>'
    print '<body onload="window.location.href = \''+redirect+'\'">'
else:
    print '<body>'
print '<div id="debug">'+dbtxt+'</div>'
print '''
<div id="warning"></div>
<div id="error"></div>
<div style="text-align:center; padding-top: 200px;">
<div style="display: inline-block">'''
if cookie:
    print ''' Logging into the system... '''
else:
    print '''
<form name="logon" id="logon" method="POST" >
<table>
<tr><td><label>User</label></td><td>:</td><td style="text-align:left;"><input type="text" name="user" id="user"/></td></tr>
<tr><td><label>Password</label></td><td>:</td><td style="text-align:left;"><input type="password" name="password" id="password"/></td></tr>'''
    print '<tr><td></td><td></td><td style="text-align:left; width:200px"><input type="submit" value="Logon for one week"  onsubmit="$(\'input#logonType\').val(\'long\')"/></td></tr>'
    print '<tr><td></td><td></td><td style="text-align:left; width:200px"><input type="submit" value="Logon for a session" onsubmit="$(\'input#logonType\').val(\'session\')"/></td></tr>'

    print '''
    </table>
    <input name="logonType" id="logonType" type="hidden" value="long"/>
    </form>'''

print '''
</div>
</div>
</body>
</html>
'''
