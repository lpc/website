#!/usr/bin/python
import json
import cgi
import os
import sys
import re
import glob
import cgitb
import pprint
import shutil
import tarfile
cgitb.enable()
from fsutils import handlefsops, checkpassword

def filterpassword( tarinfo ):
    if os.path.basename(tarinfo.name) == "password.txt":
        return None
    return tarinfo

def needpassword( userdir ):
    pwdfile = os.path.join( userdir, "password.txt")
    if os.path.isfile( pwdfile ):
        return True
    return False

    
if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form       = cgi.FieldStorage()
if 'user' in form:
    user       = form['user'].value
else:
    user = ""
action     = form['action'].value

pb = os.path.join( pp, "editorSchemes")
pp = os.path.join( pb, user )



def getSchemeList( pp, withFiles = True ):
    retdat = [{ 'name' : user, 'dpath' : "."}]
    cnode = retdat[0]
    # helper hash
    nodeh = { pp : cnode }
    for root, dirs, files in os.walk( pp ):
        # get the root in the tree structure being created
        cnode = nodeh[ root ]
        if not cnode:
            error = "cnode not "
        if withFiles:
            for nfile in files :
                if not nfile.endswith( ".json" ):
                    continue
                if not 'children' in cnode:
                    cnode['children'] = []
                path = os.path.relpath( root, pp )
                cnode['children'].append( { 'name' : nfile, 'path' : os.path.join(path, nfile) } )
        for ndir in dirs:
            if not 'children' in cnode:
                cnode['children'] = []
            dpath = os.path.relpath( root, pp )
            dpath = os.path.join( dpath, ndir );
            nnode = { 'name' : ndir,
                      'parent' : cnode['name'],
                      'dpath' : dpath }
            cnode['children'].append( nnode )
            nodeh[ os.path.join( root, ndir ) ] = nnode

    retdat[0]['password'] = needpassword(pp)
    
    if not os.path.isdir( pp ):
        retdat = []

    return retdat


schemeName = ""
retdat = []
dbx = ""
status = ""
error = ""

# only if a new user (=directory) needs to be create, the pp does not need to exist
if action <> "createDirectory" :
    if user == "" :
        error = "You must enter a valid user name"
    if not os.path.isdir( pp ):
        error = "User " + user + " does not exist"

    if error <> "":
        obj = { "debug"  : dbx,
                "error"  : error,
                "data"   : [],
                "action" : action }
        print "Content-type: application/json"
        print 
        print json.dumps( obj )
        sys.exit()

(handled, error, withFiles) = handlefsops( pp, form, action )
if handled:
    retdat = getSchemeList( pp, withFiles )

elif action == "save":
    (pwdok, error) = checkpassword(pp, form )
    if pwdok:
        direc = form['directory'].value
        schemeStr = form['schemeData'].value
        schemeName = form['schemeName'].value + ".json"
        fp = os.path.join( pp,direc,schemeName )
        # minimal security
        if re.search( r"\.\.", fp ):
            error = "Illegal operation"
        else:
            fd = open( fp, "w" )
            fd.write( schemeStr )
            fd.close()
            status = "saved"

elif action == "loadScheme":
    scheme = form['scheme'].value
    scheme = os.path.join( pp, scheme );    
    fd = open( scheme, "r" )
    retdat = fd.read()
    fd.close()

elif action == "listSchemes":
    retdat = getSchemeList( pp )

elif action == "listSchemeDirs":
    retdat = getSchemeList( pp, False )

elif action == "downloadBackup":
    tmpfile = os.path.join( pb, "backups", user + ".tgz" )
    retdat = { 'backupurl' : "editorSchemes/backups/" + user + ".tgz"}
    tfile = tarfile.open( tmpfile, "w:gz" );
    tfile.add( pp, user, filter=filterpassword )    
    tfile.close()
    
else:
    error = "No such action " + action


obj = { "debug"  : dbx,
        "error"  : error,
        "data"   : retdat,
        "action" : action,
        "status" : status }

print "Content-type: application/json"
print 
print json.dumps( obj )

