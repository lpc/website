#!/usr/bin/python
import json
import cgi
import os
import sys
import re
import cgitb

cgitb.enable()


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form = cgi.FieldStorage()

if 'user' in form:
    user = form['user'].value
else:
    user = ""

action = form['action'].value

pb = os.path.join( pp, "editorSchemes")
pp = os.path.join( pb, user )


def getSchemeList( pp, withFiles = True, recursive=True ):
    retdat = [{ 'name' : user, 'dpath' : "."}]
    cnode = retdat[0]
    # helper hash
    nodeh = { pp : cnode }
    for root, dirs, files in os.walk( pp ):
        # get the root in the tree structure being created
        cnode = nodeh[ root ]
        if not cnode:
            error = "cnode not "
        if withFiles:
            for nfile in files :
                if not nfile.endswith( ".json" ):
                    continue
                if not 'children' in cnode:
                    cnode['children'] = []
                path = os.path.relpath( root, pp )
                cnode['children'].append( { 'name' : nfile, 'path' : os.path.join(path, nfile) } )
        if recursive==False:
            break
        for ndir in dirs:
            if not 'children' in cnode:
                cnode['children'] = []
            dpath = os.path.relpath( root, pp )
            dpath = os.path.join( dpath, ndir );
            nnode = { 'name' : ndir,
                      'parent' : cnode['name'],
                      'dpath' : dpath }
            cnode['children'].append( nnode )
            nodeh[ os.path.join( root, ndir ) ] = nnode

    if not os.path.isdir( pp ):
        retdat = []

    return retdat

if action == "loadScheme":
    scheme = form['scheme'].value
    scheme = os.path.join( pp, scheme );    
    #retdat = scheme
    try:
        fd = open( scheme, "r" )
        retdat = json.load( fd )
        fd.close()
    except:
        retdat = "No such scheme : ", scheme 

elif action == "listSchemesRecursive":
    retdat = getSchemeList( pp )

elif action == "listSchemesInDir":
    retdat = getSchemeList( pp, recursive=False )

elif action == "listSchemeDirs":
    retdat = getSchemeList( pp, False )

elif action == "test" :
    retdat = { 'user' : user,
               'action' : action,
               'pp' : pp}
    
print "Content-type: application/json"
print 
print json.dumps( retdat )

