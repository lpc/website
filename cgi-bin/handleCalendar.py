#!/usr/bin/python
import json
import cgi
import os
import sys
import cgitb
from fsutils import getJsonFile, saveJsonFile

cgitb.enable()
dbx = ""
    
if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form       = cgi.FieldStorage()

# make sure we are taking the data from the root directory (i.e. from the
# production version of the site.
if os.path.basename( pp ) == "git-source":
    calpath = os.path.join( pp, "..", "documents", "ToyCalendars", "calendars.json" )
    #dbx = "git-source: "+ calpath + "   " + os.path.basename( pp )
else:    
    calpath = os.path.join( pp, "documents", "ToyCalendars", "calendars.json" )
    #dbx = "no git-source: "+ calpath + "   " + os.path.basename( pp )


action = form['action'].value

error = "no calendars"
calendars = ()

if action == "getCalendars":
    (calendars,error) = getJsonFile( calpath )
else:
    calendars = {}
    error = "Invalid request: " + action


response = { "debug"  : dbx,
             "error"  : error,
             "data"   : calendars,
             "action" : action }

print "Content-type: application/json"
print 
print json.dumps( response )

