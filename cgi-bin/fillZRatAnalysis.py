#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import json
import urllib2 


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

            
form = cgi.FieldStorage()

debug = "no debug"
error = ""
data = {}

if not "action" in form:
    error = "No action argument"
else:
    if form['action'].value == "fillCache":
        debug = "action fillData<br>"
        # get the fill data from afs
        url = "http://lpc-eos.web.cern.ch/lpc-eos/cgi-bin/getFillCache.py?fillnr=" + form['fillnr'].value + "&year=" + form['year'].value
        response = urllib2.urlopen( url );
        fillcache = json.loads(response.read())

        data = { 'action' : form['action'].value,
                 'fillnr' : form[ 'fillnr' ].value,
                 'year' : form['year'].value,
                 'cmd' : form['cmd'].value,
                 'data' : fillcache };

reply = { 'data'  : data,
          'debug' : debug, 
          'error' : error }

print "Content-type: application/json"
print 
print json.dumps( reply )

