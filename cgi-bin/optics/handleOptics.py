#!/usr/bin/python
import os
import json
import re
import cgi
import cgitb
import json
from TFSReader_nonumpy import TFSReader
from analyseLR_nonumpy import calcResult, getXRP
#from TFSReader import TFSReader
#from analyseLR import calcResult

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

twissDir = os.path.join( pp, "..", "documents", "optics" )

form = cgi.FieldStorage()

action = "no action"
error = ""
debug = ""
data = {}

if not "action" in form:
    error = "No action argument"
else:
    action = form['action'].value

if action == "chooseFile":
    filenorig = form['filename'].value
    if filenorig[-4:] == "json" :
        filenorig = form['filename'].value
        fd = open( filenorig, 'r' )
        data = json.load( fd )
        datatype = "json"
    else:
        twf1 = filenorig[0:-6] + "b1.tfs"
        twf2 = filenorig[0:-6] + "b2.tfs"
        # check if this file is a file related to an IP
        # (This is to change later in the javascript the x axis coordinates so
        #  that the 0 is at the IP)
        iptxt = filenorig[-9:-6]
        if not re.match( "ip\d", iptxt ):
            iptxt = ""
        
        tfsr = TFSReader()
        name1 = os.path.splitext(os.path.basename(twf1))[0]
        name2 = os.path.splitext(os.path.basename(twf2))[0]
        #debug = name1
        tfsr.readFile( twf1, name1 )
        tfsr.readFile( twf2, name2 )

        direc = os.path.dirname( twf1 )
        xrp = getXRP( direc, 0 );

        if re.match( r'aperture', name1 ):
            tfsr.deleteElements( name1, "ON_AP", -999999 )
            tfsr.deleteElements( name2, "ON_AP", -999999 )
        
        data = { 'ip' : iptxt,
                 'beam1' : tfsr.getData( name1 ),
                 'beam2' : tfsr.getData( name2 ) }
        datatype = "tfs"

elif action == 'calcResult':
    ip = form['ip'].value
    emittance = float(form['emittance'].value)
    directory = form['dir'].value
    datatype = 'json'
    result = calcResult( directory, ip, emittance )
    data = result['plotData']
    xrp = result['xrp']
    
reply = { 'action' : action,
          'type' : datatype,
          'error' : error,
          'debug' : debug, 
          'data' : data,
          'xrp' : xrp }

print "Content-type: application/json"
print 
print json.dumps( reply )
