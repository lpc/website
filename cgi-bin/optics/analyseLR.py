#!/usr/bin/python
import re
import sys
import math
import json
import numpy as np
import os
from pprint import pprint

from TFSReader import TFSReader

ip = "ip5"

exn = 3.5e-6
eyn = 3.5e-6

def meanbeam( sb1, sb2 ):
    x1 = sb1['X']
    x2 = sb2['X']
    z1 = sb1['Z']
    z2 = sb2['Z']

#    mx = (x1 + x2)/2
#    mz = (z1 + z2)/2

    mx = map( lambda x,y : (x+y)/2, x1, x2 )
    mz = map( lambda x,y : (x+y)/2, xz, z2 )


    d1 = np.sqrt( np.square(x1-mx) + np.square(z1-mz) )
    d2 = np.sqrt( np.square(x2-mx) + np.square(z2-mz) )

    
    # What is the sign?
    l1 = np.sqrt( np.square(x1) + np.square(z1) )
    l2 = np.sqrt( np.square(x2) + np.square(z2) )
    lm = np.sqrt( np.square(mx) + np.square(mz) )
    l1t = np.greater(l1, lm)
    l2t = np.greater(l2, lm)

    # These are the offsets which have to be added to the local coordinate system.
    off_b1 = np.where( l1t, d1, -d1)
    off_b2 = np.where( l2t, d2, -d2)

    return ( off_b1, off_b2 )
    
def transform( vec, surv ):
    ''' assumes the dimensions of the vector to be equal and the 
    elements of the vectors correspond to the same places in s'''
    #print surv
    res = { 'X' : [],
            'Y' : [],
            'Z' : []
            }
    for sx,sy,sz,theta,ss,bx,by,bs in zip( surv['X'],surv['Y'],surv['Z'],surv['THETA'],surv['S'],vec['X'],vec['Y'],vec['S'] ):
        if ss <> bs:
            #print bs-ss
            #print "s in survey and beam data not equal: this is not expected"
            sys.exit()
        # print theta
        # We will rotate by -theta

#        cang, sang = np.cos( -theta ), np.sin( -theta )
        cang = math.cos( -theta )
        sang = math.sin( -theta )

        # R = np.matrix( [[cang, -sang],[sang, cang]] )
        # rotate the local beam system (to the theta of the global system)
        # We only need to rotate the offset in x. The offset in y stays the same.
#        rpt =  np.array( [ cang * bx, sang * bx] ) # This vector is in x,z (y points upward)
        rpt = [ cang * bx, sang * bx]
        # And now we can calculate the new point with (x, y, z) : x points outside of the ring, y points upward
        x = sx + rpt[0]
        y = sy + by
        z = sz + rpt[1]
        res['X'].append( x )
        res['Y'].append( y )
        res['Z'].append( z )
        #print sy, by

    return res
    
# survey: X-Z is more or less the plane of the lhc. The global coordinate system
# seems to be a bit tilted so that y stays 0

def calcResult( direc, ip, n_emittance ): 
    tfs = TFSReader()

    # Parameters : ip, exn, eyn

    if ip == "" :
        tfs.readFile( os.path.join( direc, "marker_b1.tfs"), "b1_data" )
        tfs.readFile( os.path.join( direc, "marker_b2.tfs"), "b2_data" )
    else:
        tfs.readFile( os.path.join( direc, "ipmarker_" + ip + "b1.tfs" ), "b1_data" )
        tfs.readFile( os.path.join( direc, "ipmarker_" + ip + "b2.tfs" ), "b2_data" )

    tfs.readFile( os.path.join( direc, "survey_" + ip + "b1.tfs" ), "b1_survey" )
    tfs.readFile( os.path.join( direc, "survey_" + ip + "b2.tfs" ), "b2_survey" )

    b1s = tfs.getData( "b1_survey" )["columndata"]
    b2s = tfs.getData( "b2_survey" )["columndata"]

    b1b = tfs.getData( "b1_data" )["columndata"]
    b2b = tfs.getData( "b2_data" )["columndata"]

    params = tfs.getData( "b1_data" )['descriptors']

    # Before starting we need to compensate for any possible offset of the beams:
    # The global (00) is in IP3 where there is an offset of the reference orbits of
    # B1 and B2.
    # We proobably could avoid this by choosing IP5 as starting point. But then we
    # run into trouble with negative offsets from the IP.

    ipoff = tfs.getOffset( "b1_survey", ip.upper() )
    # ipoff is the index to the IP in the arrays

    dx = b2s['X'][ipoff] - b1s['X'][ipoff]
    #print "Need to compensate an offset of ", dx, " for beam 2."
    b2s['X'] = b2s['X'] - dx
    b2b['X'] = b2b['X']

    (b1_off, b2_off) = meanbeam( b1s, b2s )

    # Now we want to transform the markers to the global coordinate system.

    b1b['X'] = b1b['X'] + b1_off
    b2b['X'] = b2b['X'] + b2_off

    # now we can calculate the distance of the two beams at the markers

    S = b1b['S']

    soff = S[ipoff]

    S = S - soff

    ex = params['EX']
    ey = params['EY']

    #print repr( params )

    if n_emittance :
        ex = n_emittance / params['GAMMA']
        ey = n_emittance / params['GAMMA']


    bdist = np.array(map( list, zip( S, np.sqrt( np.square( b1b['X']-b2b['X'] ) + np.square( b1b['Y'] - b2b['Y'] ) ) )))

    # Now calculate the sigma of b1
    sigx_b1 = np.array(map( list, zip( S, np.sqrt( b1b['BETX'] * ex ) ) ))
    sigy_b1 = np.array(map( list, zip( S, np.sqrt( b1b['BETY'] * ey ) ) ))
    # Now calculate the sigma of b2
    sigx_b2 = np.array(map( list, zip( S, np.sqrt( b2b['BETX'] * ex ) ) ))
    sigy_b2 = np.array(map( list, zip( S, np.sqrt( b2b['BETY'] * ey ) ) ))
    # Put also beta function in result vector
    betx_b1 = np.array( map( list, zip( S, b1b['BETX'] ) ) )
    bety_b1 = np.array( map( list, zip( S, b1b['BETY'] ) ) )
    betx_b2 = np.array( map( list, zip( S, b2b['BETX'] ) ) )
    bety_b2 = np.array( map( list, zip( S, b2b['BETY'] ) ) )
    # Now the distances in sigma
    dist_sigx_b1 = zip(S, bdist[:,1] / sigx_b1[:,1])
    dist_sigx_b2 = zip(S, np.divide(bdist[:,1], sigx_b2[:,1]))

    # Now make a data structure with the results

    result = { 'rawData' : {
        'S' : S,
        'beta-x beam 1' : betx_b1,
        'beta-y beam 1' : bety_b1,
        'beta-x beam 2' : betx_b2,
        'beta-y beam 2' : bety_b2,

    },
            'plotData' : [ { 'name' : 'distance b1 b2',
                             'data' : bdist.tolist(),
                             'yAxis' : 0  },
                           { 'name' : "beta-x beam 1",
                             'data' : betx_b1,
                             'yAxis' : 0 },
                           { 'name' : "beta-x beam 1",
                             'data' : bety_b1,
                             'yAxis' : 0 },
                           { 'name' : "beta-x beam 2",
                             'data' : betx_b2,
                             'yAxis' : 0 },
                           { 'name' : "beta-x beam 2",
                             'data' : bety_b2,
                             'yAxis' : 0 },
                           { 'name' : "sigma-x beam 1",
                             'data' : sigx_b1.tolist(),
                             'yAxis' : 1  },
                           { 'name' : "sigma-y beam 1",
                             'data' : sigy_b1.tolist(),
                             'yAxis' : 1  },
                           { 'name' : "sigma-x beam 2",
                             'data' : sigx_b2.tolist(),
                             'yAxis' : 1  },
                           { 'name' : "sigma-y beam 2",
                             'data' : sigy_b2.tolist(),
                             'yAxis' : 1 },
                           { 'name' : "distance in sigma-x beam 1",
                             'data' : dist_sigx_b1,
                             'yAxis' : 2 },
                           { 'name' : "distance in sigma-x beam 2",
                             'data' : dist_sigx_b2,
                             'yAxis' : 2 },
                       ] }

    #fd = open( "results/results.json", "w" )
    #json.dump( result, fd )
    #fd.close()

    return result
