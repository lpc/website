#!/usr/bin/python
import math

def sqrt( vec ):
    return map( math.sqrt, vec)

def square( vec ):
    return map( lambda x: x*x, vec )

def greater( vec1, vec2 ):
    return map( lambda x,y: True if x>y else False, vec1, vec2 )

def where( choose, val1, val2 ):
    return map( (lambda c,x,y : x if c else y), choose, val1, val2 )

def cos( vec ):
    return map( math.cos, vec )

def sin( vec ):
    return map( math.sin, vec )

def array( arr ):
    return arr

def append( arr, el ):
    arr.append( el )
    return arr

def add( v1, v2 ):
    return map( lambda x,y:x+y, v1,v2 )

def sub( v1, v2 ):
    return map( lambda x,y:x-y, v1, v2 )

def pprint( *args ):
    length = len(args[0])
    ix = 0;
    for ix in range(0,length):
        outstr = "{:5d} : ".format( ix )
        for v in args:
            outstr += "{:12.4f}".format( v[ix] )
        print outstr
        ix +=1 

        
        
