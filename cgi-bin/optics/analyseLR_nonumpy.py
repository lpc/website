#!/usr/bin/python
import re
import sys
import math
import json
#import numpy as np
import myNumpy as np
import os
from pprint import pprint

from TFSReader_nonumpy import TFSReader

debugPrint = False

def mprint( *args ):
    if debugPrint:
        print( args )



def calcDist( sb1, sb2 ):
    dist = np.sqrt( np.add( np.add( np.square( np.sub( sb1['X'],sb2['X'] ) ), np.square( np.sub( sb1['Y'],sb2['Y'] ) ) ), np.square( np.sub( sb1['Z'],sb2['Z'] ) ) ) )
    return dist

# def meanbeam( sb1, sb2 ):
#     x1 = sb1['X']
#     x2 = sb2['X']
#     z1 = sb1['Z']
#     z2 = sb2['Z']
# 
#     mx = map( lambda x,y : (x+y)/2, x1, x2 )
#     mz = map( lambda x,y : (x+y)/2, z1, z2 )
# 
#     d1 = np.sqrt( np.add( np.square( np.sub( x1,mx ) ), np.square( np.sub( z1,mz ) ) ))
#     d2 = np.sqrt( np.add( np.square( np.sub( x2,mx ) ), np.square( np.sub( z2,mz ) ) ))
# 
# 
#     # What is the sign?
#     l1 = np.sqrt( np.add( np.square(x1) , np.square(z1) ))
#     l2 = np.sqrt( np.add( np.square(x2) , np.square(z2) ))
#     lm = np.sqrt( np.add( np.square(mx) , np.square(mz) ))
#     l1t = np.greater(l1, lm)
#     l2t = np.greater(l2, lm)
# 
#     # These are the offsets which have to be added to the local coordinate system.
#     off_b1 = np.where( l1t, d1, map( lambda x: -x, d1))
#     off_b2 = np.where( l2t, d2, map( lambda x: -x, d2))
# 
#     return ( off_b1, off_b2 )
    
def transform( vec, surv ):
    ''' assumes the dimensions of the vector to be equal and the 
    elements of the vectors correspond to the same places in s.
    The global system has its origin in Point 3 and the X-axis points
    outside of the ring, the Z-axis towards Basel and the Y-axis up.
    The global system is inclined so that the Y value of the beam does
    not change. 
    The local system is the sytem which runs on the reference orbit
    and therefore turns about the y axis when you go along the ring.
    To transform the local coordinates of the mad-x analysis to the 
    global system one has to rotate the offset in the local system 
    around the y axis by the value -theta which gives the rotation
    of the local system wrt to the global system. Then of course 
    the global offsets for the X, Y coordinates have to be added.
    '''

    res = { 'X' : [],
            'Y' : [],
            'Z' : [],
            'NAME' : []
            }

    for sx,sy,sz,theta,ss,bx,by,bs,name in zip( surv['X'],surv['Y'],surv['Z'],surv['THETA'],surv['S'],vec['X'],vec['Y'],vec['S'],vec['NAME'] ):
        if ss <> bs:
            mprint("s in survey and beam data not equal: this is not expected", ss, bs)
            sys.exit()

        # We will rotate by -theta
        cang = math.cos( -theta )
        sang = math.sin( -theta )

        # rotate the local beam system (to the theta of the global system)
        # We only need to rotate the offset in x. The offset in y stays the same.
#        rpt =  np.array( [ cang * bx, sang * bx] ) # This vector is in x,z (y points upward)
        rpt = [ cang * bx, sang * bx ]

        # And now we can calculate the new point with (x, y, z) : x points outside of the ring, y points upward
        x = sx + rpt[0]
        y = sy + by
        z = sz + rpt[1]
        res['X'].append( x )
        res['Y'].append( y )
        res['Z'].append( z )
        res['NAME'].append( name )
    return res
    
# filter out elements which are only in on beam
def filtervecs( vec1, vec2 ):
    r1 = {}
    r2 = {}
    ix1=0
    ix2=0
    vkeys = vec1.keys()
    for key in vkeys:
        r1[key]=[]
        r2[key]=[]

    while True:
        if  ix1 == len(vec1['Z']) or ix2 == len(vec2['Z']):
            return (r1,r2)
        if  vec1['NAME'][ix1] == vec2['NAME'][ix2]:
            for key in vkeys:
                r1[key].append( vec1[key][ix1] )
                r2[key].append( vec2[key][ix2] )
                ix1 += 1
                ix2 += 2
            continue

            
        mprint( vec1['NAME'][ix1], vec2['NAME'][ix2] )
        mprint ( ix1, ix2 )

        if vec1['NAME'][ix1] not in vec2['NAME']:
            # advance ix1
            ix1 += 1
            continue
        else:
            ix2 += 1
            continue



# survey: X-Z is more or less the plane of the lhc. The global coordinate system
# seems to be a bit tilted so that y stays 0
def getXRP( direc, soff ):
    tfs = TFSReader()
    tfs.readFile( os.path.join( direc, "marker_b1.tfs"), "b1_data" )
    tfs.readFile( os.path.join( direc, "marker_b2.tfs"), "b2_data" )
    b1b = tfs.getData( "b1_data" )["columndata"]
    b2b = tfs.getData( "b2_data" )["columndata"]
    romanPotsixb1 = tfs.filterElements( "b1_data", "XRP" )
    romanPotsixb2 = tfs.filterElements( "b2_data", "XRP" )
    xrp = { "b1" : [],
            "b2" : [] }

    for ix in romanPotsixb1:
        xrp['b1'].append( { 'name' : tfs.getData( "b1_data" )["columndata"]['NAME'][ix],
                            'S' : tfs.getData( "b1_data" )["columndata"]['S'][ix] - soff } )
    for ix in romanPotsixb2:
        xrp['b2'].append( { 'name' : tfs.getData( "b2_data" )["columndata"]['NAME'][ix],
                            'S' : tfs.getData( "b2_data" )["columndata"]['S'][ix] - soff } )

    return xrp
 
def calcResult( direc, ip, n_emittance ): 
    tfs = TFSReader()

    if ip == "" :
        tfs.readFile( os.path.join( direc, "marker_b1.tfs"), "b1_data" )
        tfs.readFile( os.path.join( direc, "marker_b2.tfs"), "b2_data" )
    else:
        tfs.readFile( os.path.join( direc, "ipmarker_" + ip + "b1.tfs" ), "b1_data" )
        tfs.readFile( os.path.join( direc, "ipmarker_" + ip + "b2.tfs" ), "b2_data" )

    tfs.readFile( os.path.join( direc, "survey_" + ip + "b1.tfs" ), "b1_survey" )
    tfs.readFile( os.path.join( direc, "survey_" + ip + "b2.tfs" ), "b2_survey" )

    b1s = tfs.getData( "b1_survey" )["columndata"]
    b2s = tfs.getData( "b2_survey" )["columndata"]
    
    b1b = tfs.getData( "b1_data" )["columndata"]
    b2b = tfs.getData( "b2_data" )["columndata"]

    params = tfs.getData( "b1_data" )['descriptors']


    
    # Before starting we need to compensate for any possible offset of the beams:
    # The global (00) is in IP3 where there is an offset of the reference orbits of
    # B1 and B2.
    # We proobably could avoid this by choosing IP5 as starting point. But then we
    # run into trouble with negative offsets from the IP.

    ipoff = tfs.getOffset( "b1_survey", ip.upper() )
    # ipoff is the index to the IP in the arrays
    mprint("Survey coordinates in ip: X,Z,Y")
    mprint("beam 1 ", ip, b1s['X'][ipoff],b1s['Z'][ipoff],b1s['Y'][ipoff])
    mprint("beam 2 ", ip, b2s['X'][ipoff],b2s['Z'][ipoff],b2s['Y'][ipoff])

    dx = b2s['X'][ipoff] - b1s['X'][ipoff]
    mprint("Need to compensate an offset of ", dx, " for beam 2.")
    mprint(b2s['X'])
    b2s['X'] = map( lambda x: x-dx, b2s['X'] )

    rotvec1 = transform( b1b, b1s )
    rotvec2 = transform( b2b, b2s )

    dist = calcDist( rotvec1, rotvec2 )

    # now we can calculate the distance of the two beams at the markers

    S = b1b['S']

    soff = S[ipoff]

    S = map( lambda x: x-soff, S )

    ex = params['EX']
    ey = params['EY']


    if n_emittance :
        ex = n_emittance / params['GAMMA']
        ey = ex
    else:
        ex = 3.5e-6 / params['GAMMA']
        ey = ex

    bdist = map( list, zip( S, dist) )

    # Now calculate the sigma of b1
    sigx_b1 = np.array(map( list, zip( S, np.sqrt( map( lambda x: x*ex, b1b['BETX'])) ) ))
    sigy_b1 = np.array(map( list, zip( S, np.sqrt( map( lambda x: x*ey, b1b['BETY'])) ) ))
    # Now calculate the sigma of b2
    sigx_b2 = np.array(map( list, zip( S, np.sqrt( map( lambda x: x*ex, b2b['BETX']) ) ) ))
    sigy_b2 = np.array(map( list, zip( S, np.sqrt( map( lambda x: x*ey, b2b['BETY']) ) ) ))
    # Put also beta function in result vector
    betx_b1 = np.array( map( list, zip( S, b1b['BETX'] ) ) )
    bety_b1 = np.array( map( list, zip( S, b1b['BETY'] ) ) )
    betx_b2 = np.array( map( list, zip( S, b2b['BETX'] ) ) )
    bety_b2 = np.array( map( list, zip( S, b2b['BETY'] ) ) )
    # Now the distances in sigma
    dist_sigx_b1 = zip(S, map( lambda x,y:x[1]/y[1], bdist,sigx_b1))
    dist_sigx_b2 = zip(S, map( lambda x,y:x[1]/y[1], bdist,sigx_b2))

    xrp = getXRP(direc, soff)
    
    # Now make a data structure with the results

    result = { 
        'xrp' : xrp,
        'plotData' : [ { 'name' : 'distance b1 b2',
                         'data' : bdist,
                         'yAxis' : 0  },
                       { 'name' : "beta-x beam 1",
                         'data' : betx_b1,
                         'yAxis' : 0 },
                       { 'name' : "beta-y beam 1",
                         'data' : bety_b1,
                         'yAxis' : 0 },
                       { 'name' : "beta-x beam 2",
                         'data' : betx_b2,
                         'yAxis' : 0 },
                       { 'name' : "beta-y beam 2",
                         'data' : bety_b2,
                         'yAxis' : 0 },
                       { 'name' : "sigma-x beam 1",
                         'data' : sigx_b1,
                         'yAxis' : 1  },
                       { 'name' : "sigma-y beam 1",
                         'data' : sigy_b1,
                         'yAxis' : 1  },
                       { 'name' : "sigma-x beam 2",
                         'data' : sigx_b2,
                         'yAxis' : 1  },
                       { 'name' : "sigma-y beam 2",
                         'data' : sigy_b2,
                         'yAxis' : 1 },
                       { 'name' : "distance in sigma-x beam 1",
                         'data' : dist_sigx_b1,
                         'yAxis' : 2 },
                       { 'name' : "distance in sigma-x beam 2",
                         'data' : dist_sigx_b2,
                         'yAxis' : 2 },
        ] }

    return result

################################################################################

if __name__ == "__main__":
    debugPrint = True
    res = calcResult( "./results", "ip5", 3.5e-6 )
    fd = open( "results/result.json", 'w' )
    json.dump( res['plotData'], fd )
    fd.close()
