#!/usr/bin/python
import cgitb
import cgi
import os
import re
import json
import glob
cgitb.enable()


#def procDir( cdir ):
#    filestr = ""
#    for tabfile in os.listdir( cdir ):
#        if tabfile.endswith( "tfs" ):
#            filestr += ' <option value="' + tabfile + '">'+tabfile+'</option>\n'
#    return filestr

# Contains the dirs as keys with the twiss files in an array as values
filehash = {}

def listDirs( cdir ):
    res = []
    restr = "<option></option>"
    dirs = os.listdir( cdir )

    for dirc in dirs:
        path = os.path.join( cdir, dirc )
        if not os.path.isdir( path ):
            continue
        res.append( path )
        restr += '<option value="'+str(path)+'">'+dirc+'</option>'
        tables = glob.glob(path + '/*b[12].tfs')
        tables += glob.glob(path + '/*.json')
        filehash[path] = {}
        filehash[path]['files'] = tables

        desc = ''
        if os.path.isfile( os.path.join( path, "Description.txt" ) ):
            fd = open( os.path.join( path, "Description.txt" ), 'r' )
            desc = fd.read()
            fd.close()
        filehash[path]['description'] = desc
        
    return restr



if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

cdir = os.path.join( pp, "..", "documents", "optics" )


print '''Content-Type: text/html

<!DOCTYPE HTML>
<html lang='en'>
<head>
  <title>LHC Filling Scheme Viewer</title>
  <link rel="stylesheet" type="text/css" href="../../css/lhc.css">
  <link rel="stylesheet" type="text/css" href="../../css/lpc.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
  <script src="../../js/jquery-2.2.0.min.js"></script>
  <script src="../../js/highcharts.js"></script>
  <script src="../../js/exporting.js"></script>
  <script src="../../js/offline-exporting.js"></script>
  <script src="../../js/jquery-ui.min.js"></script>
  <script src="../../js/utils.js"></script>
  <script src="../../js/optics.js"></script>
</head>
<body>
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Optics plotter
	  </p>
	  <p class="center">
            <a href="../../Default.htm">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="debug">'''
#print cdir
print '''</div>
    <div id="contents">
      <label>Select a directory: </label>
       <select id="dirsel" onchange="changeDir()">'''
dirstr = listDirs(cdir)
print dirstr
print '<script>var fileh=' + json.dumps( filehash ) + ';</script>'
print ' </select>'
print '<div style="margin-top: 20px" class="centerfixed" id="description"></div><hr width="80%"/>'
print '''
      <br/>
      <div id="resarr">
        <p onchange="recalcResults()" align="center">
          <label>Select IP : </label> 
          <select id="ipsel">
            <option value="ip1"> IP 1 </option>
            <option value="ip5"> IP 5 </option>
            <option value="ip2"> IP 2 </option>
            <option value="ip8"> IP 8 </option>
          </select>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <label>Normalised emittance (assuming round beams) : </label><input id="norm_emittance" type="number" value="3.5e-6" style="width:70px;"/>m<br>
          <button onclick="recalcResults()">plot results</button>
        </p>
        <div id="plotarea_2">
        </div>
      </div>
<hr width="80%"/>

        <table align="center">
<tr><td><label>Select a optics file </label></td><td>:</td><td>
        <select id="opticsfilesel" onchange="chooseOptics()">
        </select></td></tr>
        <tr><td><label>Select quantity to plot</label></td><td>:</td>
        <td><select id="varsel" onchange="plotvar()"></select></td></tr>
        </table>

      <div id="plotarea_1">
      </div>
   

    </div>
</body>
</html>
'''


