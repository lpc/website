#!/usr/bin/python
import re
import sys
import numpy as np

class TFSReader:
    def __init__(self): 
        self.data = {}
        

    def convFormat( self, val, format ):
        if re.match( '%l?[efgGE]', format ):
            return float( val )
        if re.match( '%l?du?', format ):
            return int(val)
        if re.match( '%[0-9]*s', format ):
            return val[1:-1]
        print "unknown format"
        print format
        print val
        sys.exit()

    def readFile( self, filename, dname ):

        if dname in self.data:
            return "data record named " + dname + "already exists"
            

        descriptors = {}
        formats = []
        names = []
        data = {}
        datarows = 0                

        tfs = open( filename, 'r' )     
        for line in tfs:
            if line[0] == '#':
                continue
            elif line[0] == '@':
                words = line.split()
                format = words[2]
                val = self.convFormat( words[3], format )
                descriptors[ words[1] ] = val
            elif line[0] == '*':
                names = line.split()[1:]
                for name in names:
                    data[name] = np.array([])
            elif line[0] == "$":
                formatarr = line.split()[1:]
                for fstr in formatarr:
                    formats.append( fstr )
            else:
                # data row
                datarr = line.split()
                for i in range( 0, len(names) ):
                    val = self.convFormat( datarr[i], formats[i] )
                    data[names[i]] = np.append( data[names[i]], [val] )
                datarows += 1 
        
        self.data[dname] = { 'name' : name,
                             'descriptors' : descriptors,
                             'columns' : names,
                             'formats' : formats,
                             'columndata' : data }

        return ""
        
    def getData( self, name ):
        if name not in self.data:
            return {}
        return self.data[name]

    def getOffset( self, name, elem ):
        cols = self.data[ name ]['columns']
        ix = 0
        for name in self.data[name]['columndata']['NAME']:
            if elem == name:
                return ix
            ix += 1
        return -1
    
if __name__ == "__main__":
    input = "../../documents/optics/twiss_ir5b1.tfs"
    tr = TFSReader()
    err = tr.readFile( input, 'ir5b1' )
    if err:
        print err
        sys.exit()
