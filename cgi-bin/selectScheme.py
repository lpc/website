#!/usr/bin/python
import json
import os
import urlparse
import cgitb
from scanScheme import scanFile, analyseScheme, findScheme

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

schemeDir = os.path.join( pp, "fillingSchemes" )

querydict = urlparse.parse_qs( str(os.environ["QUERY_STRING"]) )
error = ""
schemePath = ""
relPath = ""

if 'schemePath' in querydict:
    schemeName = querydict["schemeName"][0]
    schemePath = querydict["schemePath"][0]
    relPath    = querydict["relPath"][0]
else:
    error = "oh"
    if 'schemeName' in querydict:
        schemeName = querydict["schemeName"][0]
        if schemeName[-4:] <> ".csv":
            schemeName += ".csv"
        #error = "arrived " + schemeName + " " + schemeDir
        (schemePath,relPath,error) = findScheme( schemeName, schemeDir )
        #error = "found " + schemePath + " " + relPath
    elif 'fillNumber' in querydict:
        schemeName = findSchemeFromFill( fillNumber )
        if schemeName <> "":
            (schemePath,relPath,error) = findScheme( schemeName )
        else:
            error = "Could not find Filling Scheme for fill " + str(fillNumber)

if error == "":    
    injections = scanFile( os.path.join( schemePath, schemeName ) )
    analyseScheme( injections )
    injections['schemePath'] = schemePath
    injections['schemeName'] = schemeName
    injections['relPath']    = relPath
    injson = json.dumps( injections )
else:
    injson = json.dumps( error )

print "Content-type: application/json"
print 
#print '{ "debug" : "' + repr(schemePath) + ' -- '+ repr(injson) + '" }'
#print '{ "debug" : "' + repr(schemePath) + '", "data" : "', + injson  + '" }'
#print '{ "debug" : "'+repr(schemePath)+'", "data" : ' + injson + ' }'
print '{ "debug" : "", "data" : ' + injson + ' }'
#print '{ "debug" : "Hi there"}'

