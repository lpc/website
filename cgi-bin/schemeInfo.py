#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import re
import json
import datetime
import csv
import re
import urllib2

FILL_H = {}

dataurlbase="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" 

def makeFillHash():
    for year in [2015,2016,2017,2018]:
        dataurl = dataurlbase + str(year) + "/FillTable.json"
        infd = urllib2.urlopen( dataurl )
        filltable = json.load( infd )
        for fill,data in filltable.iteritems():
            FILL_H[ str(fill) ] = data['scheme']
                
def getFillData( filldata ):
    filename = filldata['name'] + ".csv"
    for (root,dirs,files) in os.walk( schemeDir ): 
        for filen in files:
            if filen == filename:
                fd = open( os.path.join( root, filen ) )
                fileContent = fd.read()
                fd.close()
                filldata['csv'] = fileContent
                
def returnError( text ):
    result = { 'error' : text }
    resultstr = json.dumps( result )
    print "Content-Type: application/json"
    print 
    print resultstr
    sys.exit()


RELPATTERN = re.compile( ".+(fillingSchemes.+)")

def procDir( cdir ):
    for dir in os.listdir( cdir ):
        if os.path.isdir( os.path.join( cdir,dir)) and dir.isdigit():
            mo = RELPATTERN.match( cdir );
            relpattern = "../fillingSchemes"
            if mo:
                relpattern = "../" + mo.group(1)
            print '<li data-relpath="'+ relpattern +'" data-path="'+ cdir +'">' + dir + '<ul class="noborder">'
            print "</ul></li>"

########################################################################


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

schemeDir = os.path.join( pp, "fillingSchemes" )

#fillDataDir = os.path.join( schemeDir, year )

makeFillHash()

def findSchemeName( fill ):
    pass
            
form = cgi.FieldStorage()
fmt = "json"

if "fill" in form:
    filllist = form.getlist("fill")
else:
    returnError( 'No \'fill\' query parameter in request.' )
    sys.exit(-1)

if "fmt" in form:
    fmt = form.getfirst("fmt")
    if not fmt in ["json", "download"]:
        returnError( "Unknown format: '" + fmt + "'." )

error = ""
result = { 'fills' : {} }
for fill in filllist:
    if fill in FILL_H:
        filldata = { "name" : FILL_H[ fill ] }
        getFillData( filldata )
        result['fills'][fill] = filldata
    else:
        error += ("No scheme found for fill " + fill + ". " )


if error <> "":
    result['error'] = error

if fmt == "download" :
    if len( result['fills'] ) > 1:
        returnError( "You can only download one fill at a time (only one fill parameter allowed)" )
    if 'error' in result:
        returnError( result['error'] )

    filldat = result['fills'].itervalues().next()
    filename = filldat['name'] + ".csv"

    print "Content-type: application/octet-stream"
    print "Content-Disposition: attachment; filename=\"" + filename + "\""
    print
    print filldat['csv']

    
if fmt == "json" :
    resultstr = json.dumps( result )
    
    print "Content-Type: application/json"
    print 
    print resultstr
    

