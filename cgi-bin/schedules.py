#!/usr/bin/python
import cgitb
import cgi
import os
import re


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

docDir = os.path.join( pp, "documents", "schedules" )

docPath = os.path.join( "..", "documents", "schedules" )

entries = []
for doc in sorted( os.listdir( docDir ), reverse=True ):
    mo = re.match( '.*_?(\d{4}).*\.pdf', doc )
    if mo:
        year = mo.group(1)
        path = os.path.join( docPath, doc )
        entries.append( (year,doc,path) )

print '''Content-Type: text/html

<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/lpc.css">
  <link rel="stylesheet" type="text/css" href="../css/lhc.css">
  <link rel="stylesheet" type="text/css" href="../js/themes/blue/style.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">
  <script src="../js/jquery-2.2.0.min.js"></script>
  <script src="../js/jquery-ui.min.js"></script>
  <script src="../js/jquery.tablesorter.min.js"></script>
  <script src="../js/lhctab.js"></script>
</head>
<body>
    <!-- page header -->


    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    LHC Schedules
	  </p>
	  <p class="center">
	   <a href="../Default.htm">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="debug"></div>
    <div id="contents">

      <table class="clearAndSimple" align="center">
        <thead>
          <tr><th>Year</th><th>Schedule</th></tr>
        </thead>
        <tbody>
'''  

for rec in entries:
    print "<tr><td>" + rec[0] + "</td><td><a href=\"" + rec[2] + "\">" + rec[1] + "</td></tr>" 

print '''
        </tbody>
      </table>
    </div>
  </body>
</html>
'''
