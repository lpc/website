#!/usr/bin/python

import urllib2 
import cgi
import os
import json
import cgitb
import datetime

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form = cgi.FieldStorage()

currentyear = str(datetime.date.today().year)
startyear = '2017'

# get the annotation data
if "year" in form:
    year = form["year"].value
else:
    year = currentyear
    
dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/massi_annotations.json"

annot = {}
error = ""
try:
    infd = urllib2.urlopen( dataurl )
    annot = json.load( infd )
except urllib2.HTTPError, e:
    error = "No data found for " + year

 
print '''Content-Type: text/html

<!DOCTYPE HTML>
<html lang="en">
<head> 
  <meta charset="UTF-8">
  <title>Massi File Annotations (versioning)</title>
  <link rel="stylesheet" type="text/css" href="../css/lhc.css">
  <link rel="stylesheet" type="text/css" href="../css/lpc.css">
  <link rel="stylesheet" type="text/css" href="../js/themes/blue/style.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">
  <script src="../js/jquery-2.2.0.min.js"></script>
  <script src="../js/jquery-ui.min.js"></script>
  <script src="../js/jquery.tablesorter.min.js"></script>
  <script src="../js/lhctab.js"></script>
  <script>
  doit = function() {
    $('table#annotTable').tablesorter( );
  }
  $.fn.directtext = function() {
    var str = '';

    this.contents().each(function() {
        if (this.nodeType == 3) {
            str += this.textContent || this.innerText || '';
        }
    });
    return str;
  };

  </script>
</head>


<body onload="doit()">
    <!-- page header -->


    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Massi File Annotations (versioning)
	  </p>
	  <p class="center">
	   <a href="../Default.htm">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error">'''
print error
print '''</div>
    <div id="debug"></div>
    <div id="contents" class="minutes-text">
<h2>Massi File versioning in the experiments</h2>
<p>
Each experiments develops its appropriate versioning system according to the guidelines
in the <a href="../MassiFileDefinition_v2.htm" target="_blank">Massi File Specifications</a>.
Below, the versioning systems of the various experiments are explained. This information
is extracted automatically from the Massi files. On the bottom of the page a table 
lists the versions of the current Massi files for each fill number in each experiment.
</p>
<p>
Massi file versioning is available as of 2017.
</p>
<p>
All of the information can be downloaded in form of a JSON file from the following 
URL (e.g. for scripting):<br> '''
print '<a href="https://lpc-afs.web.cern.ch/lpc-afs/LHC/' + year + '/massi_annotations.json">'
print 'https://lpc-afs.web.cern.ch/lpc-afs/LHC/' + year + '/massi_annotations.json</a>'
print '</p>'
print '''<script>
var baseu = location.protocol + '//' + location.host + location.pathname
</script>
'''
print '<p><select class="topselector" id="yearselect" onChange="window.location.href=baseu+\'?year=\'+$(\'#yearselect\').val()">'
for oy in reversed(range(int(startyear),int(currentyear)+1)):
    sel = ""
    if oy == int(year):
        sel = "selected"
    print '<option value="'+str(oy)+'" '+sel+'>'+str(oy)+'</option>'
print '</select></p>'

print '<table id="massiannot">'
print '''<colgroup>
   <col style="background-color: #ffffb0;">
   <col style="background-color: #e0ffd0;">
</colgroup>
''' 
print '<tr style="border-bottom: 1px solid black;"><th style="width:50%">Luminosity version annotation</th><th>Lumi-region version annotation</th></tr>'

def printTable(tag, exp, annot ):
    if annot == {}:
        return
    print '<span  class="bold">'
    print exp + '</span><br><span class="italic">'
    dann = annot[exp][tag]['digit_annotation']
    vann = annot[exp][tag]['value_annotation']

    dstr = repr(vann)
    #print vann
    skeys = sorted(dann.keys(), key=int)

    point = ''
    digit = 'k'
    vstr = ""
    for key in skeys:
        vstr += point + digit
        digit = chr(ord(digit)+1)
        point = '.'
    print vstr + " [ _number(s) ... ]</span>"

    print '''<table class="massidigits" style="border: 1px solid black;">
<tr><th class="borderbottom">digit</th><th class="borderbottom"></th><th class="borderbottom">meaning and possible values</th></tr>'''
    digit = 'k'
    for key in skeys :
        print '<tr><td class="digit">' + digit + "</td><td>:</td><td>" + dann[str(key)] + "<br>"
        digit = chr(ord(digit)+1)
        try:
            pvals = vann[unicode(key)]
            pvk = sorted( pvals.keys(), key=int)

            print '<table class="massivalues"><tr><th class="digit">value</th><th></th><th>meaning</th></tr>'

            for pk in pvk:
                print '<tr><td class="digit">' + pk + '</td><td>:</td><td>' + pvals[unicode(pk)] + '</td></tr>'

                
            print '</table>'
        except :
            print "<em>inconsistent annotation file</em> " 
        print "</td></tr>"
    print '</table>'
    

for exp in [ 'ALICE','ATLAS','CMS','LHCb' ]:
    print '<tr><td>'
    printTable('lumi', exp, annot )
    print '</td><td>'
    printTable( 'lumireg', exp, annot )
    print '</td>'
    print '</tr>'        

print '</table>'

def printVersionTable( tag, annot ):
    if annot == {}:
        return
    print '''
        <div>
          <table id="annotTable" class="tablesorter" style="border: 1px solid grey; width:auto; margin:auto;">
            <thead>
               <tr><th style="min-width:50px">Fill No</th><th style="min-width:50px">ALICE</th><th style="min-width:50px">ATLAS</th><th style="min-width:50px">CMS</th><th style="min-width:50px">LHCb</th></tr>
            </thead>
            <tbody>
    '''
    
    vh = annot['fills'][tag]
    for fillno in sorted( vh.keys(), key=int ):
        versions = vh[unicode(fillno)]
        print '<tr><td>'+str(fillno)+'</td><td>'+versions['ALICE']+"</td><td>"+versions['ATLAS']+'</td><td>'+versions['CMS']+'</td><td>'+versions['LHCb']+'</td></tr>'
    
    print '''
            </tbody>
          </table>
        </div>'''

print "<p>&nbsp;</p>"
print "<hr style='width:80%;'>"
print "<p>&nbsp;</p>"

print '<table style="width:100%;">'
print '''<colgroup>
   <col style="background-color: #ffffb0;">
   <col style="background-color: #e0ffd0;">
</colgroup>
''' 
print "<tr>"
print '<th>'+year+' Massi-Luminosity-File Versions</th><th>' + year + ' Massi-Luminosity-Region-File Versions</th></tr>'
print '<tr><td>'
printVersionTable('lumi', annot)
print "</td><td>"
printVersionTable('lumireg', annot)
print '''
    </td></tr></table>
  </div>
</body>
</html>
'''

#print '''
#    <div>
#      <table id="annotTable" class="tablesorter" style="border: 1px solid grey; width:auto; margin:auto;">
#        <thead>
#           <tr><th style="min-width:50px">Fill No</th><th style="min-width:50px">ALICE</th><th style="min-width:50px">ATLAS</th><th style="min-width:50px">CMS</th><th style="min-width:50px">LHCb</th></tr>
#        </thead>
#        <tbody>
#'''
#
#vh = annot['fills']['lumi']
#for fillno in sorted( vh.keys(), key=int ):
#    versions = vh[unicode(fillno)]
#    print '<tr><td>'+str(fillno)+'</td><td>'+versions['ALICE']+"</td><td>"+versions['ATLAS']+'</td><td>'+versions['CMS']+'</td><td>'+versions['LHCb']+'</td></tr>'
#
#print '''
#        </tbody>
#      </table>
#    </div>

