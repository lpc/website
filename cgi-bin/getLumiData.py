#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import json
import urllib2 


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

            
form = cgi.FieldStorage()
if "year" in form:
    year = form["year"].value
else:
    year = str(datetime.date.today().year)

dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/lumidata.json"
zraturl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/zCountingData.json"
turnaroundurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/turn_around_cache.json"

try:
    infd = urllib2.urlopen( dataurl )
    jsondoc = infd.read()
except URLError:
    jsondoc = "{}"

zjson = "{}"
error = ""

try:
    infz = urllib2.urlopen( zraturl )
    zjson = infz.read()
except urllib2.URLError:
    zjson = "{}"
    error = "No z-ratio data"
except:
    error = "Unexpected error:" + sys.exc_info()[0]
    zjson = "{}"

try:
    tac = urllib2.urlopen( turnaroundurl )
    tajson = tac.read()
except urllib2.URLError:
    tajson = "{}"
    error = "No turnaround data"
except:
    error = "Unexpected error:" + sys.exc_info()[0]
    tajson = "{}"

# now get the marker data
tablename = "EventAnnotations_" + year + ".json"
tablepath = os.path.join( pp, "documents", "EventAnnotations", tablename )
markers = []

if os.path.isfile( tablepath ):
    fd = open( tablepath, 'r' )
    markers = json.load( fd )
    fd.close()

data = json.loads( jsondoc )
data['markers'] = markers
data['zratio'] = json.loads( zjson )
data['turnaround'] = json.loads( tajson )
data['error'] = error

jsondoc = json.dumps( data )

print "Content-type: application/json"
print 
print jsondoc
