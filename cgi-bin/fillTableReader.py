#!/usr/bin/python
import json
import cgi
import os
import glob
import cgitb
import re
import datetime
import shutil
import urllib2 

cgitb.enable()

if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

form = cgi.FieldStorage()

# globals

pdir = os.path.join( pp, "documents", "FillTables")
action = form['action'].value
error = ""



def loadTable():
    global error
    
    # get the filltable generated from timber            
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    dataurl="http://lpc-afs.web.cern.ch/lpc-afs/LHC/" + year + "/FillTable.json"
    try:
        infd = urllib2.urlopen( dataurl )
        jsondoc = infd.read()
    except urllib2.HTTPError, e:
        error = repr(e)
        jsondoc = "{}"
        
    timbertab = json.loads( jsondoc )

    # Now get the annotated fill table which we want to edit here
    tablename = "FillTable_" + year + ".json"
    tablepath = os.path.join( pdir, tablename )
    #error = tablepath
    anntab = {}
    if os.path.isfile( tablepath ):
        fd = open( tablepath, "r" )
        ats = fd.read()
        anntab = json.loads( ats )
    
    # Now add all non existing entries of the timber table into the Fill Table
    for fill, entry in timbertab.iteritems() :
        if fill not in anntab:
            anntab[ fill ] = entry
#        else:
#            anntab[ fill ]['length_sb'] = entry['length_sb']

    return (anntab, year)


if action == 'load' :
    (data, year) = loadTable();
    res = { "status" : "loaded", 
            "action" : action,
            "data"  : data,
            "year"  : year,
            "error" : error }

else:
    error =  "action " + action + " not known"
    res = { "status" : "problem" }
    

print "Content-type: application/json"
print 
print json.dumps( res )

