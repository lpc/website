#!/usr/bin/python
import cgitb
import cgi
import os
import sys
import json
import urllib2 


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

            
form = cgi.FieldStorage()

debug = "no debug"
error = ""
data = {}

if not "action" in form:
    error = "No action argument"
else:
    if form['action'].value == "fillData":
        debug = "action fillData<br>"
        # get the fill data from afs
        url = "http://lpc-eos.web.cern.ch/lpc-eos/cgi-bin/getFillData.py?fillnr=" + form['fillnr'].value + "&year=" + form['year'].value + "&exp=" + form['exp'].value
        response = urllib2.urlopen( url );
        filldata = json.loads(response.read())
        


        data['fillData'] = { 'action' : form['action'].value,
                             'fillnr' : form[ 'fillnr' ].value,
                             'year' : form['year'].value,
                             'exp' : form['exp'].value,
                             'data' : filldata };

reply = { 'data'  : data,
          'debug' : debug, 
          'error' : error }

print "Content-type: application/json"
print 
print json.dumps( reply )

