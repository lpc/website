#!/usr/bin/python
import json
import cgi
import os
import glob
import cgitb
import sys

cgitb.enable()


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

debug = ""
error = ""
status = ""

form       = cgi.FieldStorage()
user       = form['user'].value
action     = form['action'].value
etype      = form['type'].value

pb = os.path.join( pp, "editorSchemes")
pp = os.path.join( pb, user )

def checkUser():
    if not os.path.isdir( pp ):
        error = "User \"" + user + "\" does not exist."
        retdat = { 'status' : "ERROR",
                  'error'  : error,
                  'data'   : {}
              }
        print "Content-type: application/json"
        print 
        print json.dumps( retdat )
        sys.exit()

checkUser();

if action == "loadBatches":
    bfile = os.path.join( pp, etype + ".dat" );
    if os.path.isfile( bfile ):
        fd = open( bfile, 'r' )
        retdat = json.load( fd )
        fd.close()
        status = "loaded"
    else:
        retdat = {}

elif action == "removeBatch" :
    pname   = form['pname'].value
    bfile   = os.path.join( pp, etype + ".dat" )
    batches = {}
    if os.path.isfile( bfile ) :
        fd      = open( bfile, 'r' )
        batches = json.load( fd )
        fd.close()

    if pname in batches:
        del batches[pname]
        fd = open( bfile, 'w' )
        json.dump( batches, fd )
        fd.close()
        status = "deleted"
    else:
        status = 'unknown pattern "' + pname + '"'

    retdat = batches
        
elif action == "saveBatch" :
    pname   = form['pname'].value
    pattern = json.loads(form['pattern'].value)
    bfile   = os.path.join( pp, etype + ".dat" )
    batches = {}
    if os.path.isfile( bfile ) :
        fd      = open( bfile, 'r' )
        batches = json.load( fd )
        fd.close()

    batches[pname] = pattern
    fd = open( bfile, 'w' )
    json.dump( batches, fd )
    fd.close()
    
    status = "saved"
    retdat = batches


    

obj = { "debug"  : debug,
        "error"  : error,
        "status" : status,
        "data"   : retdat,
        "action" : action }

print "Content-type: application/json"
print 
print json.dumps( obj )

