#!/usr/bin/python

import json
import os
import sys
import glob
#import sys
import urlparse
import re

debug = ""

#querystring = str(os.environ["QUERY_STRING"])[13:]
querydict = urlparse.parse_qs( str(os.environ["QUERY_STRING"]) )
querystring = querydict["searchstring"][0]

#for key in os.environ:
#    debug += key + " : " + os.environ[key] + "<br>"

#for key in querydict:
#    debug += key + " :" + querydict[key][0] +  ":<br>"


TAG_RE = re.compile(r'<[^>]+>')
def remove_tags(text):
    return TAG_RE.sub('', text)


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, "lpc-minutes/*.htm" )

files = glob.glob(pp)

matches = 0
items = {}

regex = r'('+querystring+')'
try:
    SE_RE = re.compile(regex, re.I | re.M | re.S)
except:
    error = "Not a valid python regular expression"
    print "Content-type: application/json"
    print 
    print '{ "error" : "' + error + '" }'
    sys.exit(0)
    

for fn in files:

    content = open(fn).read()
    content = remove_tags( content )

    mo = re.search( SE_RE, content ) 

    if mo :
        matches += 1
        start = mo.start(0) - 200
        if start < 0:
            start = 0
        end = mo.end(0) + 200
        if len(content) < end:
            end = len(content)

        extract = "..." + content[start:end] + "..."
        extract = SE_RE.sub( '<span class="searchitem">\\1</span>',extract )

        item = { "sys"  : extract,
                 "link" : "lpc-minutes/" + os.path.basename(fn) }
        key = (os.path.basename( fn ))[:-4]
        items[ key ] = item

        
data = { "matches"     : matches,
         "items"       : items,
         "querystring" : querystring }


print "Content-type: application/json"
print 
print '{ "debug" : "' + debug + '", "data" : '+ json.dumps( data ) +' }'
